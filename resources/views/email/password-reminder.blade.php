@extends('email.layout')
@section('content')

    <p>Reset your password by clicking <a href={{$url}}>here</a></p>

@stop

@section('signature')
    <p>Dustyn and Jamie,<br>
        OnlineMedEd</p>
@stop

@extends('layouts.email')
@section('content')

<p style="text-align: center;">Activate your OnlineMedEd account by clicking <a href="{{$url}}">here</a>.</p>

@stop

@section('signature')
@stop
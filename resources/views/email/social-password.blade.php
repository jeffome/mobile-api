@extends('email.layout')
@section('content')
    <p>Thank you for signing up. Since you signed up with social media, you'll still need to set a password if you want to log in with your email address.</p>
    <p>Set your password by clicking <a href={{$url}}>here</a></p>

@stop

@section('signature')
    <p>Dustyn and Jamie,<br>
        OnlineMedEd</p>
@stop

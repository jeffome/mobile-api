<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"></html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>OnlineMedEd</title>
    <style type="text/css">
        /* Based on The MailChimp Reset INLINE: Yes. */
        /* Client-specific Styles */
        #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes.*/
        .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        /* Forces Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
        #backgroundTable {margin: 10px 0 25px 0; padding:0; width:100% !important; line-height: 1.5em !important;}
        /* End reset */

        /* Some sensible defaults for images
        Bring inline: Yes. */
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        .image_fix {display:block;}

        /* Yahoo paragraph fix
        Bring inline: Yes. */
        p {margin: 1em 0;}

        /* Hotmail header color reset
        Bring inline: Yes. */
        h1, h2, h3, h4, h5, h6 {color: black !important;}

        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
            color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
        }

        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
            color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
        }

        /* Outlook 07, 10 Padding issue fix
        Bring inline: No.*/
        table td {border-collapse: collapse;}

        /* Remove spacing around Outlook 07, 10 tables
        Bring inline: Yes */
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

        /* Styling your links has become much simpler with the new Yahoo.  In fact, it falls in line with the main credo of styling in email and make sure to bring your styles inline.  Your link colors will be uniform across clients when brought inline.
        Bring inline: Yes. */
        a { color:blue; }

    </style>
</head>
<body style="background-color:#EFEFEF;text-align:center;width:100%;">
    <table id="backgroundTable" cellpadding="0" cellspacing="0" border="0" style="background-color:#EFEFEF;text-align:center;width:100%;">
        <tr>
            <td valign="top" style="padding:25px 0;">
                <table cellpadding="0" cellspacing="0" border="0" align="center" style="width:80%;margin:0 auto;">
                    <tr>
                        <td valign="top" style="background-color:#FFFFFF;text-align:center;padding:0 20px;">
                            <br><br>
                            <img src="https://onlinemeded.org/images/redesign/logo_1x.png" alt="OnlineMedEd" title="OnlineMedEd" width="321" height="41" style="margin:25px auto;" class="image_fix"/></td>
                    </tr>
                    <tr>
                        <td valign="top" style="font-size:16px;color:#333333;font-family:Helvetica,Arial,Sans-Serif;padding:20px;background-color:#FFFFFF;text-align:left;">

                            @section('content')
                            @show

                            @section('signature')
                            @show

                            @section('footer')
                            @show
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"style="text-align:center;font-size:12px;color:#333333;font-family:Helvetica,Arial,Sans-Serif;">
                            <tablealign="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top">
                                        <table align="right">
                                            <tr>
                                                <td style="text-align: right;">
                                                    <br>
                                                    <a href="https://twitter.com/onlinemeded"><img src="https://onlinemeded.org/images/social/email/twitter.png" alt="Twitter" height="25" width="25" style="border:0" /></a>
                                                    <a href="https://www.facebook.com/OnlineMedEd/"><img src="https://onlinemeded.org/images/social/email/fb.png" alt="Facebook" height="25" width="25" style="border:0" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

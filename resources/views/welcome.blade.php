<!DOCTYPE html>
<html>
    <head>
        <title>
            OnlineMedEd API
        </title>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                margin: 0 20px;
            }

            img {
                max-width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img alt="Online Med Ed" src="https://onlinemeded.org/images/redesign/logo_2x.png">
                </img>
            </div>
        </div>
    </body>
</html>

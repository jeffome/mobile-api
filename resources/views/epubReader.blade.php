<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="{{asset('mix-manifest.json')}}">
        <link rel="stylesheet" type="text/css" href="{{mix('css/epubReader.css')}}">
        <title>OME EpubReader</title>
    </head>
    <body>
        <div
            id="epubReader"
            data-url="{{ $url }}"
            data-title="{{ $title }}"
        ></div>
        <script src="{{mix('js/epubReaderLoader.js')}}" ></script>
    </body>
</html>

# OnlineMedEd Flashback API

## Getting Started

### Passport

Flashback uses Passport for communicating with the OME Main repository. To configure this bridge, run the following commands.

#### In Main:
```
php artisan passport:client --name=FlashbackAPI --password
```
#### In Flashback
Copy the printed values from the above command into the `.env` file in `flashback-api` under the following keys:
```
OME_PASSPORT_CLIENT_ID=
OME_PASSPORT_CLIENT_SECRET=
```
Run `php artisan ome:fetch-passport-token` to obtain the authorization Bearer token and save it in the `OME_PASSPORT_BEARER_TOKEN` field in the `.env` file.

To test that the credentials have been successfully configured run:
```
vendor/bin/phpunit --filter it_should_successfully_connect_to_ome_main_with_passport
```


### DB Setup & Migrations

This API uses the production OME database structure, so all migrations should be created and executed on the main project at git@bitbucket.org/onlinemeded/onlinemeded.

### Assets

Install dependencies
```
composer install
```

### Tests

ALWAYS ALWAYS ALWAYS ALWAYS write unit tests for EVERY EVERY EVERY new feature.

```
phpunit
```

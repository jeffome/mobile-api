<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{

    protected $table = 'flashback_feedback';

    protected $fillable = ['user_id', 'feedback'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

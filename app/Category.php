<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{
    public function flashcards()
    {
        return $this->hasMany('App\Flashcard');
    }
    
    public function questions()
    {
        return $this->hasMany('App\QBankQuestion');
    }

    public function topics()
    {
        return $this->hasMany('App\Topic');
    }

    public function clerkship()
    {
        return $this->belongsTo('App\Clerkship');
    }

    public function scopeStatsData(Builder $query) {
        return $query
                ->withCount([
                    'topics' => function($query) {
                        $query->where('is_active_flashback', 1);
                    },
                    'flashcards' => function($query) {
                        $query->where('is_active_flashback', 1);
                    },
                    'questions' => function($query) {
                        $query->where('status', 'Approved');
                    },
                ])
                ->with(['topics' => function($query) {
                        $query->where('is_active_flashback', 1);
                    }]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DigitalBook extends Model
{
    protected $table = 'digital_books'; 
}

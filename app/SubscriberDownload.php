<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberDownload extends Model
{
    protected $dates = [
        'time'
    ];

    protected $fillable = ['time', 'topic_id', 'type', 'filepath', 'user_id', 'is_remediation'];

    // Avoid Eloquent expecting 'updated_at' to exist
    public $timestamps = false;
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SocialPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $url;
    
    public function __construct($code, $user)
    {
        $this->url = config('main.url') . "/reset/" . $user->id . "/" . $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Set your account password.')
                    ->view('email.social-password');
    }
}

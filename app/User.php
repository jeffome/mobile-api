<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Customer;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivateEmail;
use Redis;

class User extends Authenticatable
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'last_login',
        'last_active',
        'flashback_trial_end_date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'year',
        'school_text_id',
        'password',
        'career_track_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $subscription        = null;
    private $subscriptionEndDate = null;

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

    public function careerTrack() {
        return $this->hasOne(Track::class,'id','career_track_id');
    }

    public function downloadlimits() {
        return $this->hasMany(DownloadLimit::class);
    }

    public function downloads() {
        return $this->hasMany(SubscriberDownload::class);
    }

    public function socials() {
        return $this->hasMany(Social::class);
    }

    public function reports() {
        return $this->hasMany(ErrorReport::class);
    }

    public function feedback() {
        return $this->hasMany(Feedback::class);
    }

    public function recentTopics() {
        return $this->hasMany(TopicHistory::class);
    }

    public function studiedTopics() {
        return $this->hasMany(TopicStudied::class);
    }

    public function deckStates() {
        return $this->hasMany(DeckState::class);
    }

    public function customDecks() {
        return $this->hasMany(CustomDeck::class);
    }

    public function setting() {
        return $this->hasOne(UserSetting::class);
    }

    public function answers() {
        return $this->hasMany(UserAnswer::class);
    }

    public function dailyAnswerTallies() {
        return $this->hasMany(DailyAnswerTally::class);
    }

    public function systemDecks() {
        return $this->hasMany(SystemDeck::class);
    }

	/**
	 * determines if a user has a particular permission to access a product-based permission given a certain track
	 */
	public function trackProductPermissions()
	{
		return $this->hasMany(TrackProductPermissions::class);
	}

    public function markedqbanks() {
        return $this->belongsToMany(QBankQuestion::class, 'markedqbank_user', 'user_id', 'qbank_id');
    }

    public function activation()
    {
        return $this->hasOne(Activation::class, 'user_id', 'id');
    }

    public function getSettings() {

        $setting = UserSetting::firstOrCreate([
            'user_id' => $this->id,
        ]);

        return $setting->fresh(); // required so the default values are actually included
    }


    public function getSystemDeckByTypeId($systemDeckTypeId) {
        if ($systemDeckTypeId) {
            return SystemDeck::where([
                'user_id' => $this->id,
                'flashcard_system_deck_type_id' => $systemDeckTypeId
            ])->first();
        }

        return null;
    }

    // downloads
    public function getDownloadCount(int $trackId, bool $updateCache) {
        $key = 'user:' . $this->getKey() . ':track:' . $trackId . ':downloadsLeft';
        if (!$updateCache && Redis::exists($key) && !empty($currentValue = Redis::get($key))) {
            return $currentValue;
        }
        $downloadsLeft = 0;
        $result = $this->downloadLimits->where('track_id', $trackId)->first();
        if (is_object($result)) {
            $downloadsLeft = $result->limit - $result->total_unique;
        }

        $downloadsLeft = max(0, $downloadsLeft);

        Redis::setex($key, 5, $downloadsLeft); // 5 seconds

        return (int)Redis::get($key);
    }

    /** @deprecated  */
    public function updateDownloadMetadata($payload = [])
    {
        $url = config('main.url') . "/api/v1/user/{$this->id}/download-limit";
        $accessToken = ApiKey::byProperty('flashback-api');
        $response = (new Client)->post($url, [
            'form_params' => $payload,
            'headers' => [
                'Accept'        => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
        ]);
        return $response->getBody()->getContents();
    }

    // purchases
    public function hasPurchasedUnlimitedDownloads()
    {
        $output = Customer::getPurchases($this, 'virtual');
        if ($output && $purchases = $output->products) {
            foreach ($purchases as $purchase) {
                if ($purchase->product && $purchase->product->sku == 'unlimited_downloads') {
                    return true;
                }
            }
        }
        return false;
    }

    public function activate()
    {
        if ($this->activation) {
            $this->activation->completed = true;
            $this->activation->completed_at = Carbon::now();
            $this->activation->save();
        }
    }

    /**
     * Check if the user is activated
     *
     * @return bool
     */
    public function isActivated(): bool
    {
        /** @var Activation $activated */
        $activated = $this->activation()->first();

        return !(!$activated || !$activated->completed);
    }

    public function hasMatchingPendingActivationCode($code)
    {
        return $this->activation && !$this->activation->completed && $this->activation->code === $code;
    }

    /**
     * Send the activation email.
     *
     * @param  string $activationCode
     */
    public function emailActivate($activationCode)
    {
        Mail::to($this)->send(new ActivateEmail($activationCode, $this));
    }

    public function activateWithoutEmail() {
        $activation = $this->activation()->create([], [
                'code' => Activation::getToken(),
                'created_at' => Carbon::now(),
                'completed_at' => Carbon::now(),
                'completed' => true,
        ]);
    }

    public function sendActivationEmail()
    {
        if ($this->shouldSendActivationEmail()) {
            $activation = $this->activation()->updateOrCreate([], [
                'code' => Activation::getToken(),
                'created_at' => Carbon::now()
            ]);

            $this->emailActivate($activation->code);
        }
    }

    public function shouldSendActivationEmail()
    {
        return !$this->isActivated() || $this->shouldResendActivationEmail();
    }

    public function shouldResendActivationEmail()
    {
        $activation = $this->activation()->first();

        return !$activation || ($activation && Carbon::now()->gte(optional($activation)->created_at->addDay()));
    }

	public function downloadsLimit($track = 2)
	{
		$result = $this->downloadLimits()->where('track_id', $track)->first();
		if ($result) {
			return $result->limit * $this->dl_cap_multiplier;
		}
		return null;
    }

	public function hasUnlimitedDownloads($trackIds = [2])
	{
		return $this->checkPermission($trackIds, 'can-access-unlimited-downloads');
	}

	private function checkPermission($trackIds, $permissionName)
	{
		// Typecast trackIds to array
		is_array($trackIds) ? : $trackIds = [$trackIds];

		$permission = Permission::where('name', $permissionName)->first();

		if ($permission) {
			return $this->trackProductPermissions()
				->whereIn('track_id', $trackIds)
				->where('permission_id', $permission->id)
				->exists();
		}

		return false;
	}

	/**
	 * WARNING READ ME:
	 * The "real" hasUnlimitedDownloads() method has a return value based on the user's current
	 * permissions. This works just fine as long as the permissions query is isolated to the
	 * user's currently ACTIVE track. If you try to query permissions for an inactive
	 * track, you will get inaccurate results. Which is the basis for this method
	 * of unlimited download detection.
	 *
	 * @param $track_id
	 * @return boolean
	 */
	public function hasUnlimitedDownloadsForTrack($track_id): bool
	{
		return Cache::remember(
			__METHOD__ . ':user:' . $this->getKey(). ':track:' . $track_id,
			Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY,
			function () use ($track_id) {
				$response = optional(\Customer::getPurchases($this, 'virtual'))['response'];
				$products = optional($response)->products;

				return collect($products)
					->map(function ($product) {
						return $product->product;
					})
					->where('client_track_id', $track_id)
					->contains('type', 'unlimited_downloads');
			}
		);
	}

	public function downloadsLeft($trackId = 2, $updateCache = false)
	{
		$key = 'user:' . $this->getKey() . ':track:' . $trackId . ':downloadsLeft';
		if (!$updateCache && Redis::exists($key) && !empty($currentValue = Redis::get($key))) {
			return $currentValue;
		}
		$downloadsLeft = 0;
		$result = $this->downloadLimits->where('track_id', $trackId)->first();
		if (is_object($result)) {
			$downloadsLeft = $result->limit - $result->total_unique;
		}

		$downloadsLeft = max(0, $downloadsLeft);

		Redis::setex($key, 5, $downloadsLeft); // 5 seconds

		return (int)Redis::get($key);
	}

    public function hasFreePremium()
    {
        return $this->hasFreeExternalPremium() || $this->hasFreeInternalPremium();
    }

    public function hasFreeExternalPremium()
    {
        $freePremiumPlans = ['free-external'];

        return in_array($this->rsubplan, $freePremiumPlans) && $this->rsubstate == 'active';
    }

    public function hasFreeInternalPremium()
    {
        $freePremiumPlans = ['free', 'free-internal'];

        return in_array($this->rsubplan, $freePremiumPlans) && $this->rsubstate == 'active';
    }

    public function doesSystemDeckExistForUser($systemDeckId)
    {
        $systemDeckType = SystemDeckType::where('is_configurable', true)->findOrFail($systemDeckId);
        $deckExist = false;

        if ($systemDeckType && $systemDeckType->is_configurable) {
            $deck = SystemDeck::firstOrCreate([
                'user_id'                       => $this->id,
                'flashcard_system_deck_type_id' => $systemDeckType->id,
            ]);

            if ($deck) $deckExist = true;
        }

        return $deckExist;
    }

}

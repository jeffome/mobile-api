<?php

namespace App\Transformers;

use App\Topic;

class TopicTransformer extends Transformer
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Topic $topic)
    {
        return [
            'id'            => (int) $topic->id,
            'name'          => $topic->name,
            'description'   => $topic->description,
            'nickname'      => $topic->nickname,
            'slug'          => $topic->slug,
            'searchterms'   => $topic->searchterms,
            'sort'          => (int) $topic->sort,
            'category_id'   => (int) $topic->category_id,
            'is_active'     => $topic->is_active_flashback == 1,
            'free_video_id' => $topic->free_video_id,
            'note_url'      => $topic->getWrappedUrl('notes'),
            'audio_url'     => $topic->getWrappedUrl('audio'),
            'whiteboard_url'=> $topic->getWrappedUrl('whiteboard'),
            'video_url'     => $topic->getWrappedUrl('video'),
            'video_cc_url'  => $topic->getWrappedUrl('video_cc'),
            'created_at'    => $this->convertServerTimeToUTC($topic->created_at),
            'updated_at'    => $this->convertServerTimeToUTC($topic->updated_at),
            'keyconcepts'   => $topic->keyconcepts,
        ];
    }
}

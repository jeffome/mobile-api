<?php

namespace App\Transformers;

use App\CustomDeck;
use App\Transformers\Transformer;
use App\User;

class CustomDeckTransformer extends Transformer
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(CustomDeck $deck)
    {  
        return [
            'id'            => (int) $deck->id,
            'user_id'       => (int) $deck->user_id,
            'name'          => $deck->name,
            'slug'          => $deck->slug,
            'flashcard_ids' => $deck->flashcardsByTrack()->pluck('flashcards.id'),
            'created_at'    => $this->convertServerTimeToUTC($deck->created_at),
            'updated_at'    => $this->convertServerTimeToUTC($deck->updated_at),
        ];
    }
}

<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class Transformer extends TransformerAbstract
{

    /**
     * Returns the date converted from local server timezone to UTC
     * @param  Carbon  $stamp
     * @param  boolean $dateTime true include time, false only date
     * @return string
     */
    protected function convertServerTimeToUTC($stamp, $dateTime = true)
    {
        if (is_null($stamp)) {
            return null;
        } else {
            if (is_string($stamp)) {
                $stamp = Carbon::createFromTimeString($stamp);
            }

            if ($dateTime) {
                return $stamp->setTimezone('UTC')->toDateTimeString();
            } else {
                return $stamp->setTimezone('UTC')->toDateString();
            }
        }
    }
}

<?php

namespace App\Transformers;

use App\Flashcard;
use App\Module;
use App\QBankQuestion as Question;
use App\QBankResult;
use App\Transformers\Transformer;

class ResultTransformer extends Transformer
{

    /**
     * Prepare result model for API export
     *
     * @return array
     */
    public function transform(QBankResult $result)
    {
        return [
            'id'                => (int) $result->id,
            'user_id'           => (int) $result->user_id,
            'qbank_id'          => (int) $result->qbank_id,
            'answer'            => (int) $result->answer,
            'result'            => (string) $result->result,
            'is_correct'        => $result->is_correct,
            'created_at'        => $this->convertServerTimeToUTC($result->created_at),
            'updated_at'        => $this->convertServerTimeToUTC($result->updated_at),
        ];
    }
}

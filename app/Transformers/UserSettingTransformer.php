<?php

namespace App\Transformers;

use App\Transformers\Transformer;
use App\UserSetting;

class UserSettingTransformer extends Transformer
{

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(UserSetting $settings)
    {
        return [
            'user_id'        => (int) $settings->user_id,
            'include_step_2' => $settings->include_step_2 == 1,
            'include_step_3' => $settings->include_step_3 == 1,
            'timer_7_day'    => $settings->timer_7_day == 1,
            'timer_14_day'   => $settings->timer_14_day == 1,
            'timer_30_day'   => $settings->timer_30_day == 1,
            'created_at'     => $this->convertServerTimeToUTC($settings->created_at),
            'updated_at'     => $this->convertServerTimeToUTC($settings->updated_at),
        ];
    }
}

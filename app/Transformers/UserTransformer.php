<?php

namespace App\Transformers;

use App\User;

class UserTransformer extends Transformer
{

	/**
	 * Turn this item object into a generic array
	 *
	 * @return array
	 */
	public function transform(User $user)
	{
		$settings = $user->getSettings();

		return [
			'id'                => (int)$user->id,
			'first_name'        => $user->first_name,
			'last_name'         => $user->last_name,
			'email'             => $user->email,
			'profile_photo_url' => $user->imageurl,
			'career_track_id'   => $user->career_track_id,
			'socials'           => $user->socials->pluck('provider'),
			'downloads'         => [
				'basic-sciences' => [
					'download_limit'          => $user->downloadsLimit(1),
					'has_unlimited_downloads' => $user->hasUnlimitedDownloadsForTrack(1),
					'remaining_downloads'     => $user->downloadsLeft(1, true),
					'track_id'                => 1,
				],
				'clinical'       => [
					'download_limit'          => $user->downloadsLimit(),
					'has_unlimited_downloads' => $user->hasUnlimitedDownloadsForTrack(2),
					'remaining_downloads'     => $user->downloadsLeft(2, true),
					'track_id'                => 2,
				],
			],
			'settings'          => [
				'user_id'        => (int)$settings->user_id,
				'include_step_2' => $settings->include_step_2 == 1,
				'include_step_3' => $settings->include_step_3 == 1,
				'timer_7_day'    => $settings->timer_7_day == 1,
				'timer_14_day'   => $settings->timer_14_day == 1,
				'timer_30_day'   => $settings->timer_30_day == 1,
				'is_activated'   => $user->isActivated(),
			],
		];
	}
}

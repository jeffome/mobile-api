<?php

namespace App\Transformers;

use App\Flashcard;
use App\Module;
use App\QBankQuestion as Question;
use App\Transformers\Transformer;
use App\User;

class ModuleTransformer extends Transformer
{
    protected $getQuestions = true;
    protected $moduleResults = true;
    protected $shouldShufleQuestionIds = true;

    protected function shuffleQuestionIds($questionids) {
        if (!$this->shouldShufleQuestionIds) {
            return $questionids;
        }

        $qids = explode(',', $questionids);
        shuffle($qids);
        return implode(',', $qids);
    }

    public function noQuestions() {
        $this->getQuestions = false;
    }

    public function globalResults() {
        $this->moduleResults = false;
    }

    public function noQuestionIdsShuffle() {
        $this->shouldShufleQuestionIds = false;
    }

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Module $module)
    {
        return [
            'id'                => (int) $module->id,
            'user_id'           => (int) $module->user_id,
            'display_name'      => (string) $module->display_name,
            'questionids'       => (string) $this->shuffleQuestionIds($module->questionids),
            'testmode'          => (string) $module->testmode,
            'questionmode'      => (string) $module->questionmode,
            'numquestions'      => (int) $module->countQuestions(),
            'numcorrect'        => (int) $module->getNumcorrectAttribute(),
            'complete'          => (int) $module->complete,
            'starttime'         => (int) $module->starttime,
            'timerlength'       => (int) $module->timerlength,
            'finishtime'        => (int) $module->finishtime,
            'active'            => (int) $module->active,
            'created_at'        => $this->convertServerTimeToUTC($module->created_at),
            'updated_at'        => $this->convertServerTimeToUTC($module->updated_at),
            'answer_attempts'   => json_decode($module->answer_attempts),
            'questions'         => $module->getQuestionsAttribute($this->getQuestions, $this->moduleResults),
        ];
    }
}

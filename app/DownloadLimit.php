<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class DownloadLimit extends Model
{
    protected $table = 'download_limits';

    protected $fillable = ['limit', 'total', 'total_unique', 'user_id', 'track_id'];

    public function tracks()
    {
        return $this->belongsTo('App\Track');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}

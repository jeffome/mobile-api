<?php

namespace App;

use DateTime;
use DB;
use Request;
use Illuminate\Database\Eloquent\Model;

class UserMetric extends Model
{
    protected $table = 'user_metrics';
    protected $fillable = [
        'user_id',
        'type',
        'object_id',
        'is_remediation',
        'ip',
        'description',
        'updated_at',
    ];
    /**
     * returns true if the metric already exists for a given user_id, type, and object_id
     * @param  int $user_id      user_id
     * @param  string $type      type (case, video, ...)
     * @param  int $object_id    object_id (case id, video id, ...)
     * @return boolean
     */
    private static function metricAlreadyExists($user_id, $type, $object_id, $isRemediation = 0)
    {
        return self::where('user_id', $user_id)
            ->where('type', $type)
            ->where('object_id', $object_id)
            ->where('is_remediation', $isRemediation)
            ->first();
    }

    public static function saveNewMetric($user_id, $type, $object_id, $description, $override_creation_date = false, $isRemediation = false)
    {
        // TODO: this method is not being used, figure out if we still need this, if not delete or replace
        $exists = self::metricAlreadyExists($user_id, $type, $object_id, $isRemediation);
        // If the type is a case and the case has not already been viewed today, allow duplicate entries.
        if (($type == 'case' || $type == 'flashcard') && $exists && (date('m/d/Y') != date('m/d/Y', strtotime($exists->created_at)))) {
            $exists = false;
        }

        if ($exists) {
            return false;
        } else {
            $metric                 = new self();
            $metric->user_id        = $user_id;
            $metric->object_id      = $object_id;
            $metric->type           = $type;
            $metric->is_remediation = $isRemediation;
            $metric->description    = $description;

            // save the ip address if available
            if (class_exists('Request') && Request::ip()) {
                $metric->ip = Request::ip();
            }

            if ($override_creation_date !== false) {
                $metric->created_at = $override_creation_date;
            }

            $metric->save();

            return $metric;
        }
    }
  }
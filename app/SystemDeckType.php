<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemDeckType extends Model
{
    protected $table = 'flashcard_system_deck_types';

    protected $fillable = ['id', 'name', 'slug', 'is_configurable'];

    const DEFAULT_TYPES = [
        1 => [
            'name' => 'Studied Up',
            'slug' => 'studied-up',
            'is_configurable' => 0,
        ],
        2 => [
            'name' => 'Saved Cards',
            'slug' => 'saved-cards',
            'is_configurable' => 1,
        ],
        3 => [
            'name' => 'Hard Ones',
            'slug' => 'hard-ones',
            'is_configurable' => 1,
        ]
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (!$this->exists && !$this->slug) {
            $this->attributes['slug'] = str_slug($value);
        }
    }

    /**
     * Ensure that all default deck types exist
     */
    public static function ensureDefaultDeckTypesExist() {
        foreach (self::DEFAULT_TYPES as $id => $updates) {
            SystemDeckType::updateOrCreate(['id' => $id], $updates)->save();
        }

        return SystemDeckType::all();
    }

}

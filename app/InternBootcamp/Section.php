<?php

namespace App\InternBootcamp;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'intern_bootcamp_sections';

    protected $appends = ['href'];

    public function getHrefAttribute()
    {
        return '/intern-bootcamp/' . $this->slug;
    }

    /**
     * The videos that belong to the section.
     */
    public function videos()
    {
        return $this->hasMany('App\InternBootcamp\Video', 'intern_bootcamp_section_id')->orderBy('sort');
    }
}

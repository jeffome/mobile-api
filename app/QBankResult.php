<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class QBankResult
 *
 * @SWG\Definition(title="Result", type="object", description="User object returned by API")
 *
 */
class QBankResult extends Model
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="user_id", type="integer"),
     * @SWG\Property(property="qbank_id", type="integer"),
     * @SWG\Property(property="answer", type="integer"),
     * @SWG\Property(property="is_correct", type="boolean"),
     * @SWG\Property(property="is_remediation", type="boolean"),
     * @SWG\Property(property="remediation_id", type="integer"),
     * @SWG\Property(property="challenge_id", type="integer"),
     * @SWG\Property(property="created_at", type="string"),
     * @SWG\Property(property="updated_at", type="string"),
     */

    protected $table = 'results';
    protected $appends = ['is_correct'];

    const CORRECT = 'C';
    const WRONG = 'W';

    protected $fillable = [
        "user_id",
        "qbank_id",
        "answer",
        "result",
        "is_correct",
    ];

    protected $guarded = ['id'];

    public function question()
    {
        return $this->belongsTo('App\QBankQuestion');
    }

    public function getIsCorrectAttribute()
    {
        $this->attributes['is_correct'] = $this->result == QBankResult::CORRECT;
        return +($this->attributes['is_correct']);
    }
}

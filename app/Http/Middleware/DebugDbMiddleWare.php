<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;

class DebugDbMiddleWare
{
    public function __construct() {
        $this->enabled = Config::get('database.debugging.slowQueryLog.enabled');
        $this->threshold = Config::get('database.debugging.slowQueryLog.threshold');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->enabled) {
            \DB::enableQueryLog();
        }
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store or dump the log data...
        if ($this->enabled) {
            $logs = DB::getQueryLog();
            foreach($logs as $queryLog) {
                if ($queryLog['time'] > $this->threshold) {
                    $prep = $queryLog['query'];
                    foreach( $queryLog['bindings'] as $binding ) {
                        $prep = preg_replace("#\?#", is_numeric($binding) ? $binding : "'" . $binding . "'", $prep, 1);
                    }
                    error_log('SLOW   ' . $queryLog['time'] . '   ' . $prep);
                }
            }
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Social;
use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Carbon\Carbon;
use GuzzleHttp;
use function GuzzleHttp\json_decode;
use JWTAuth;
use Validator;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware(
            ['jwt.auth', 'enforceCareerTrack'],
            ['except' => [
                'authenticateFromEmail',
                'authenticateFromSocial'
            ],
        ]);
    }

    /**
     * Retrieve user model from token
     *
     * @Parameter \Illuminate\Http\Request $request
     * @Response \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/authenticate",
     *     tags={"Authenticate"},
     *     @SWG\Parameter(
     *          name="token",
     *          in="query",
     *          description="User token",
     *          required=true,
     *          type="string",
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/User")),
     *   ),
     * )
     */
    public function index()
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Authenticate user from email and password
     *
     * @Parameter \Illuminate\Http\Request $request
     * @Response \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/authenticate",
     *     tags={"Authenticate"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *                  example="pete@onlinemeded.org"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *                  example="medipass"
     *              )
     *          )
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="successful operation",
     *     @SWG\Schema(ref="#/definitions/User")),
     *   ),
     * )
     */
    public function authenticateFromEmail(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        // grab user data to pass out with token
        $user = User::where('email', $credentials['email'])->first();
        if(!$user) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
        $xformer = new UserTransformer;
        $data = $xformer->transform($user);
        // @REMOVEME hack to fix how the app sends unencoded credentials
        $credentials['email'] = str_replace(' ', '+', $credentials['email']);

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            \Bugsnag::notifyException($e);
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token', 'data'));
    }

    /**
     * Authenticate user from social credentials
     *
     * @Parameter \Illuminate\Http\Request $request
     * @Response \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/authenticate/social",
     *     tags={"Authenticate"},
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="uid",
     *                  type="string",
     *              ),
     *              @SWG\Property(
     *                  property="provider",
     *                  type="string",
     *              ),
     *              @SWG\Property(
     *                  property="oauth2_token",
     *                  type="string",
     *              ),
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *              )
     *          )
     *     ),
     *   @SWG\Response(
     *      response=200,
     *      description="successful operation",
     *      @SWG\Schema(ref="#/definitions/User")),
     *   ),
     * )
     */
    public function authenticateFromSocial(Request $request)
    {

        $validator = Validator::make($request->only('uid', 'provider', 'oauth2_token', 'email'), [
            'uid' => 'required',
            'provider' => 'required',
            'oauth2_token' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], 400);
        }

        $credentials = $request->only('uid', 'provider', 'oauth2_token', 'email');

        // there is a linked social account
        $check = $this->checkSocialCredentials($credentials);

        if (!$check['is_valid']) {
            // linked social account is not valid delete the record (unlink)
            Social::where('uid', $credentials['uid'])
                ->where('provider', strtolower($credentials['provider']))
                ->delete();

            return response()->json([
                'message' => 'Invalid provider crendentials '.$credentials['provider'],
                'error' => $check,
            ], 401);
        }

        $user = User::where('email', strtolower($credentials['email']))->first();

        if (!$user) {
            // there is no user with that email address
            $social = Social::where('uid', $credentials['uid'])
                ->where('provider', strtolower($credentials['provider']))
                ->first();
        
            if (!$social) {
                return response()->json([
                    'message' => 'Invalid provider crendentials '.$credentials['provider'],
                    'error' => null,
                ], 401);
            }

            $socialLinkedUser = User::where('id', strtolower($social->user_id))->first();
            
            if (!$socialLinkedUser) {
                // no ome account associated also delete the social record (unlink)
                $social->delete();
                return response()->json([
                    'message' => 'No account associated with provider credentials '.$credentials['provider'],
                    'error' => null,
                ], 401);
            }

            // validate user
            $userTransformer = new UserTransformer();
            return response()->json([
                'token' => JWTAuth::fromUser($socialLinkedUser),
                'data' => $userTransformer->transform($socialLinkedUser),
            ], 200);
        }

        $social = Social::where('uid', $credentials['uid'])
            ->where('user_id', $user->id)
            ->where('provider', strtolower($credentials['provider']))
            ->first();
                    
        if ($social) {
            // there is a linked account with user provided email adress
            $userTransformer = new UserTransformer();
            return response()->json([
                'token' => JWTAuth::fromUser($user),
                'data' => $userTransformer->transform($user),
            ], 200);
        }
        
        $social = Social::where('user_id', $user->id)
            ->where('provider', strtolower($credentials['provider']))
            ->first();
 
        if ($social) {
            // There is another social credential linked with this account
            return response()->json([
                'message' => 'This email address is already linked to a different '.$credentials['provider'].' account',
                'error' => null,
            ], 401);
        }

        $social = Social::where('uid', $credentials['uid'])
            ->where('provider', strtolower($credentials['provider']))
            ->first();
 
        if ($social) {
            // this social credential is linked to a different email adress account
            $socialLinkedUser = User::where('id', strtolower($social->user_id))->first();
                
            if (!$socialLinkedUser) {
                // no ome account associated also delete the social record (unlink)
                $social->delete();
                return response()->json([
                    'message' => 'No account associated with provider credentials '.$credentials['provider'],
                    'error' => null,
                ], 401);
            }

            // validate user
            $userTransformer = new UserTransformer();
            return response()->json([
                'token' => JWTAuth::fromUser($socialLinkedUser),
                'data' => $userTransformer->transform($socialLinkedUser),
            ], 200);
        }

        // there is no account linked with credentials, is safe to link the account.
        $social = Social::create([
            'user_id' => $user->id,
            'uid' => $credentials['uid'],
            'provider' => $credentials['provider'],
            'oauth2_access_token' => $credentials['oauth2_token'],
            'oauth2_expires' => Carbon::now()->addMonths(2),
        ]);

        // validate user
        $userTransformer = new UserTransformer();
        return response()->json([
            'token' => JWTAuth::fromUser($user),
            'data' => $userTransformer->transform($user),
        ], 200);
    }

    public static function checkSocialCredentials($credentials) {
        if ($credentials['provider'] == 'facebook') {
            try {
                $client = new GuzzleHttp\Client([
                    'base_uri' => 'https://graph.facebook.com/me',
                    'exceptions' => false,
                ]);

                $response = $client->get('?fields=id,email&access_token=' . urlencode($credentials['oauth2_token']));
                $body = json_decode($response->getBody());
                
                return [
                    'is_valid' => property_exists($body, 'id') && $body->id == $credentials['uid'],
                    'data' => $body,
                ];
                
            } catch (Exception $e) {
                return [
                    'is_valid' => false,
                    'error' => $e,
                ];
            }

        }
        
        if ($credentials['provider'] == 'google') {
            try {
                $client = new GuzzleHttp\Client([
                    'base_uri' => 'https://www.googleapis.com',
                    'exceptions' => false,
                ]);

                $response = $client->get('/oauth2/v1/tokeninfo?access_token=' . urlencode($credentials['oauth2_token']));

                $body = json_decode($response->getBody());

                return [
                    'is_valid' => property_exists($body, 'user_id') && $body->user_id == $credentials['uid'],
                    'data' => $body,
                ];

            } catch (Exception $e) {
                return [
                    'is_valid' => false,
                    'error' => $e,
                ];
            }
        }

        return [
            'error' => 'Invalid Provider',
            'is_valid' => false
        ];
    }
}

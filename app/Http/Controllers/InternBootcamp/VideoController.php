<?php

namespace App\Http\Controllers\InternBootcamp;

use App\Http\Controllers\Controller;
use App\InternBootcamp\Video;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class VideoController extends Controller
{
    public function index() {
        $videos = Video::where('is_coming_soon', 0)->where('is_active', 1)->get();

        foreach ($videos as $key=>$video) {
            $video->video_cc_url = "/intern-bootcamp/assets/video_cc/$video->id";
        }

        return response()->json([
            'data' => $videos
        ]);
    }

    public function show($section) {
        return response()->json([
            'data' => Video::find($section),
        ]);
    }

    public function getVideoClosedCaption($id) {
        $video = Video::where('id', $id)->first();

        if (!$video) {
            return response()->json([
                'message' => 'Could not find video',
                'error' => null,
            ], Response::HTTP_NOT_FOUND);    
        }

        $jwplayerUrl = 'https://cdn.jwplayer.com/v2/media/'. $video['video_id'];
        $json = json_decode(file_get_contents($jwplayerUrl), true);

        $captionUrl = null;
        
        if (isset($json['playlist']) && count($json['playlist'] >= 1)) {
            if(isset($json['playlist'][0]['tracks']) && count($json['playlist'][0]['tracks'])) {
                foreach ($json['playlist'][0]['tracks'] as $key => $value) {
                    if ($value['kind'] == 'captions')  $captionUrl = $value['file'];
                }
            };
        }

        if (!$captionUrl) {
            return response()->json([
                'message' => 'Closed captions not available for this video',
                'error' => null,
            ], Response::HTTP_NOT_FOUND);    
        }

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/srt', true);
        $response->setCallback(function () use($captionUrl) {
            readfile(str_replace(' ', '%20', $captionUrl));
            flush();
        });

        return $response;
    }
}

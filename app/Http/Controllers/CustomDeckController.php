<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\Collection;
use App\Transformers\CustomDeckTransformer;
use Carbon\Carbon;
use App\CustomDeck;
use App\Flashcard;
use App\User;
use Validator;

class CustomDeckController extends Controller
{
    /**
     * Responds to requests to GET /custom-decks
     */
    public function index(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);
        $decks = $user->customDecks()->get();

        return new Collection($decks, new CustomDeckTransformer($user));
    }

    /**
     * Responds to requests to GET /custom-decks/<id>
     */
    public function show($id)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);
        $deck = $user->customDecks()->findorFail($id);
        return $this->response->item($deck, new CustomDeckTransformer);
    }

    /**
     * Responds to requests to POST /custom-decks
     */
    public function store(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $payload = $request->only(
            'name',
            'flashcard_ids',
            'created_at',
            'updated_at'
        );

        $validator = Validator::make($payload, [
            'name' => 'required|string',
            'flashcard_ids' => 'array',
            'created_at' => 'date',
            'updated_at' => 'date'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Could not create new custom deck',
                'error' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        if (empty($payload['flashcard_ids'])) {
            $payload['flashcard_ids'] = [];
        }

        $deck = CustomDeck::create([
            'name' => $payload['name'],
            'user_id' => $user->id,
        ]);

        // allow overriding the created at or updated at field
        if (isset($payload['created_at']) || isset($payload['updated_at'])) {
            if (isset($payload['created_at'])) {
                $created_at = $this->convertUtcToServerLocal($payload['created_at']);
                $deck->created_at = $created_at->toDateTimeString();
            } else {
                $deck->created_at = Carbon::now();
            }

            if (isset($payload['updated_at'])) {
                $updated_at = $this->convertUtcToServerLocal($payload['updated_at']);
                $deck->updated_at = $updated_at->toDateTimeString();
            } else {
                $deck->updated_at = Carbon::now();
            }

            $deck->save();
        }

        // get all the deck flashcards that don't belong to this career track;
        $trackFlashcards = $deck->flashcardsByTrack()->get();
        $deck->flashcards()->detach($trackFlashcards);

        // remove repeated ids if any
        $flashcardIds = array_unique($payload['flashcard_ids']);
        // Ensure that the flashcard ids reference real flashcards
        $updatedTrackFlashcards = Flashcard::whereIn('id', $flashcardIds)->get();

        // attach updated track flashcards to total flashcards
        $deck->flashcards()->attach($updatedTrackFlashcards);

        return $this->response->item($deck, new CustomDeckTransformer);
    }

    /**
     * Responds to requests to DELETE /custom-decks/<id>
     */
    public function destroy($id)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);
        $deck = $user->customDecks()->findorFail($id);

        // remove any associations
        $deck->flashcards()->sync([]);

        $deck->delete();

        $response = [
            'message' => "The custom deck with id `$id` was deleted successfully",
        ];

        return $response;
    }

    /**
     * Responds to requests to POST /custom-decks
     */
    public function update($id, Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);
        $userTrack = $user->career_track_id;

        $payload = $request->only('name', 'flashcard_ids', 'updated_at');

        $validator = Validator::make($payload, [
            'name' => 'required|string',
            'flashcard_ids' => 'array',
            'updated_at' => 'date'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Could not update custom deck',
                'error' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        $deck = $user->customDecks()->findorFail($id);

        $deck->name = $payload['name'];

        // allow overriding the created at field
        if (isset($payload['updated_at'])) {
            $updated_at = $this->convertUtcToServerLocal($payload['updated_at']);
            $deck->updated_at = $updated_at->toDateTimeString();
        } else {
            $deck->updated_at = Carbon::now();
        }

        $deck->save();

        if (empty($payload['flashcard_ids'])) {
            $payload['flashcard_ids'] = [];
        }

        // get all the deck flashcards that don't belong to this career track;
        $trackFlashcards = $deck->flashcardsByTrack()->get();
        $deck->flashcards()->detach($trackFlashcards);
            
        // remove repeated ids if any
        $flashcardIds = array_unique($payload['flashcard_ids']);
        // Ensure that the flashcard ids reference real flashcards
        $updatedTrackFlashcards = Flashcard::whereIn('id', $flashcardIds)->get();

        // attach updated track flashcards to total flashcards
        $deck->flashcards()->attach($updatedTrackFlashcards);

        return $this->response->item($deck, new CustomDeckTransformer($user));
    }
}

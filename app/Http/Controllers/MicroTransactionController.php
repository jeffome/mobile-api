<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserMetric;
use App\TopicHistory;
use Config;
use DB;
use Validator;
use Customer;
use Cart;
use Log;
use Product;
use Stripe\Stripe;
use Stripe\Charge;

class MicroTransactionController extends Controller
{
    public function postMicroTransaction(Request $request) {
        try {
            $user = app('Dingo\Api\Auth\Auth')->user();
            $user = User::findOrFail($user->id);

            $payload = $request->only('token', 'vendor', 'product_id');

            $validator = Validator::make($payload, [
                'token'         => 'required',
                'vendor'            => 'required',
                'product_id'            => 'integer|required',
            ])->validate();

            Stripe::setApiKey(Config::get('services.stripe.secret'));

            $product = Product::find($payload['product_id']);
            if (!$product['ok']) {
                return response()->json([
                    'error' => 'Product not found',
                ]);
            }

            $priceInCents = $product['response']->price * 100;

            $payment = Charge::create(array(
                "amount" => $priceInCents,
                "currency" => "usd",
                "source" => $payload['token'],
                "description" => "Test of microtransactions"
            ));

            if(!$payment->paid) {
                return response()->json([
                    "error" => $payment->failure_message,
                ]);
            }
            $customer = Customer::getOrCreate($user);
            $checkout = Cart::checkoutThirdParty($customer, [$payload['product_id']], $payload['vendor'], $payment->id, date("Y-m-d"));

            return response()->json([
                "data" => [
                    "success" => true,
                    "paymentId" => $payment->id,
                    "checkout" => $checkout,
                    "PriceInCents" => $priceInCents,
                ]
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                "error" => "An unexpected error occured: " . $e->getMessage()
            ]);
        }
        
    }
}

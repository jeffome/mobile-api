<?php

namespace App\Http\Controllers;

use App\ApiKey;
use App\Topic;
use App\Transformers\TopicTransformer;
use App\Http\Resources\Collection;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use GuzzleHttp\Client;

class TopicController extends Controller
{
    /**
     * Responds to requests to GET /topics
     */
    public function index(Request $request)
    {
        $topics = $this->getUserCareerTrack()
                             ->getTopics()
                             ->get();

        return new Collection($topics, new TopicTransformer);        
    }

    private function getVideoSource($videoId, $size)
    {
        $jwplayerUrl = 'https://cdn.jwplayer.com/v2/media/' . $videoId;
        $json        = json_decode(file_get_contents($jwplayerUrl), true);
        foreach ($json['playlist'][0]['sources'] as $source) {
            if (isset($source['height']) && $source['height'] == $size) {
                return $source['file'];
            }
        }
    }

    private function getVideoCloseCaption($videoId)
    {
        $jwplayerUrl = 'https://cdn.jwplayer.com/v2/media/' . $videoId;
        $json        = json_decode(file_get_contents($jwplayerUrl), true);

        $captionUrl = null;

        if (isset($json['playlist']) && count($json['playlist']) >= 1) {
            if (isset($json['playlist'][0]['tracks']) && count($json['playlist'][0]['tracks'])) {
                foreach ($json['playlist'][0]['tracks'] as $key => $value) {
                    if ($value['kind'] == 'captions') {
                        $captionUrl = $value['file'];
                    }
                }
            };
        }

        return $captionUrl;
    }

    private function getAudioSource($videoId)
    {
        $jwplayerUrl = 'https://cdn.jwplayer.com/v2/media/' . $videoId;
        $json  = json_decode(file_get_contents($jwplayerUrl), true);
        foreach ($json['playlist'][0]['sources'] as $source) {
            if ($source['type'] === 'audio/mp4') {
                return $source['file'];
            }
        }
    }

    private function getUrlFromTopic($contentType, Topic $topic)
    {
        switch ($contentType) {
            case 'video':
                return $this->getVideoSource($topic->free_video_id, 720);
            case 'video_cc':
                return $this->getVideoCloseCaption($topic->free_video_id);
            case 'audio':
                return $this->getAudioSource($topic->free_video_id);
            case 'notes':
                return $topic->getNoteUrlAttribute();
            case 'whiteboard':
                return $topic->getWhiteboardUrlAttribute();
            default:
                return null;
        }
    }

    private function getPathForTracking($contentType, $topic)
    {
        $sansExtension = str_replace([':', '/'], '', $topic->category->name . ' - ' . $topic->name);

        switch ($contentType) {
            case 'video':
                return "/file/video/$topic->id." . $this->getFileExt($contentType);
            case 'video_cc':
                return "/file/video_cc/$topic->id." . $this->getFileExt($contentType);
            case 'audio':
                return "/file/audio/$topic->id/$sansExtension." . $this->getFileExt($contentType);
            case 'notes':
                return "/file/notes/$topic->id/$sansExtension." . $this->getFileExt($contentType);
            case 'whiteboard':
                return "/file/whiteboard/$topic->id/$sansExtension." . $this->getFileExt($contentType);
        }
    }

    private function getFileExt($contentType)
    {
        switch ($contentType) {
            case 'video':
                return 'mp4';
            case 'video_cc':
                return 'srt';
            case 'audio':
                return 'mp3';
            case 'notes':
                return 'pdf';
            case 'whiteboard':
                return 'pdf';
        }
    }

    public function getResponseContentType($contentType)
    {
        switch ($contentType) {
            case 'video':
                return 'video/mp4';
            case 'video_cc':
                return 'text/srt';
            case 'audio':
                return 'audio/mpeg';
            case 'notes':
                return 'application/pdf';
            case 'whiteboard':
                return 'application/pdf';
        }     
    }

    public function getAsset($contentType, $topicId, $download = false)
    {
        /** @var User $user */
        $user  = $this->getUser();
        $topic = Topic::findOrFail($topicId);
        $url   = $this->getUrlFromTopic($contentType, $topic);
        $trackId = $this->getUserCareerTrack()->id;

        if ($url === null)
        {
            return response()->json([
                'message' => 'Unsupported Content-Type',
                'error'   => null,
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($download)
        {
            $response = new StreamedResponse();
            $response->headers->set('Content-Type', $this->getResponseContentType($contentType));

            $downloadMetaData = $user->updateDownloadMetadata([
                'topic_id' => $topic->id,
                'user_id'  => $user->id,
                'type'     => $contentType,
            ]);
    
            $jsonMetaData = json_decode($downloadMetaData);

            $response->headers->set('X-OME-DOWNLOAD-LIMITS', $downloadMetaData);

            if (!optional($jsonMetaData)->canDownload)
            {
                return $response->setStatusCode(Response::HTTP_NO_CONTENT)
                                ->setCallback(function () {});
            }

            $response->setCallback(function () use ($url) {
                $sanitazedUrl = str_replace(' ', '%20', $url);
                readfile($sanitazedUrl);
                flush();
            });

            return $response;
        }
        
        if ($contentType === 'audio' || $contentType === 'notes' || $contentType === 'whiteboard')
        {
            $downloadMetaData = $user->updateDownloadMetadata([
                'topic_id' => $topic->id,
                'user_id'  => $user->id,
                'type'     => $contentType,
            ]);
    
            $jsonMetaData = json_decode($downloadMetaData);

            if(!optional($jsonMetaData)->canDownload)
            {
                return response()->json([
                    'message' => 'Maximum allowed views reached',
                    'error'   => null,
                ], Response::HTTP_FORBIDDEN);
            }
        }

        return redirect()->away($url);
    }

    public function getAssetUrl($contentType, $topicId)
    {
        /** @var User $user */
        $user = $this->getUser();
        $topic = Topic::findOrFail($topicId);
        $url = $this->getUrlFromTopic($contentType, $topic);

        if ($url === null) {
            return response()->json([
                'message' => 'Unsupported Content-Type',
                'error' => null,
            ], Response::HTTP_BAD_REQUEST);
        }

        $client = new Client([
            'base_uri' => config('main.url')
        ]);
        $accessToken = ApiKey::byProperty('flashback-api');
        $response = $client->get("api/v1/user/$user->id/download-limit/topic/$topicId/type/$contentType", [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
        ]);
        $downloadLimits = json_decode($response->getBody());

        return response()->json([
            'url' => $url,
            'download_limits' => $downloadLimits,
        ]);
    }

    public function confirmDownload($contentType, $topicId)
    {
        if (env('BETA_DISABLE_CONFIRM_DOWNLOAD', false)) {
            return '{
    "basic-sciences": {
        "download_limit": null,
        "has_unlimited_downloads": true,
        "remaining_downloads": 30,
        "track_id": 1
    },
    "clinical": {
        "download_limit": null,
        "has_unlimited_downloads": true,
        "remaining_downloads": 30,
        "track_id": 2
    }
}';
        }

        /** @var User $user */
        $user = $this->getUser();
        $client = new Client([
            'base_uri' => config('main.url')
        ]);
        $accessToken = ApiKey::byProperty('flashback-api');
        $payload = [
            'type' => $contentType,
            'topic_id' => $topicId,
            'user_id' => $user->id,
        ];
        $response = $client->post('api/v1/user/' . $user->id . '/download-limit', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer {$accessToken}",
            ],
            'form_params' => $payload,
        ]);
        return $response->getBody()->getContents();
    }

}

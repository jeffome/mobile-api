<?php

namespace App\Http\Controllers;

use App\Module;
use App\QBankQuestion;
use App\QBankResult;
use App\Transformers\ModuleTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ModuleController extends Controller
{
    /**
     * Returns all Named and 190 Unnamed Question Modules with optional updated_since
     *
     * @param \Illuminate\Http\Request $request ex: updated_since = 11-5-2018
     * @return \Dingo\Api\Http\Response
     */
    public function getAllModules(Request $request) {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        // Get user modules
        $user_modules = Module::where('user_id', $user->id)->get();
        $unnamed_modules = $user_modules->where('settings->display_name', null)->sortByDesc('updated_at')->take(10);
        $named_modules = $user_modules->where('settings->display_name', '!=', null);

        // Merge and sort module collections all display name modules, first 10 unnamed
        $modules = $named_modules->merge($unnamed_modules)->sortByDesc('settings->display_name');

        // Handle optional updated since parameter
        if ($request->updated_since) {
            $updatedSinceTimestamp = date('Y-m-d H:i:s', strtotime($request->updated_since.'UTC')); // this time specify UTC
            $modules = $modules->whereDate('updated_at', '>', $updatedSinceTimestamp);
        }

        // Setup module transformer and disable question retrieval
        $transformer = new ModuleTransformer();
        $transformer->noQuestions();
        $transformer->noQuestionIdsShuffle();

        $inTrackModules = $modules->filter(function($module) {
            return count($module->getTrackFilteredQuestionIds()) > 0;
        });

        return $this->response->collection($inTrackModules, $transformer);
    }

    /**
     * Returns a Question Module based on ID
     *
     * @param $module_id
     * @return \Dingo\Api\Http\Response
     *
     */
    public function getModulesById($module_id) {
        $module = Module::findOrFail($module_id);

        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();
        return $this->response->item($module, $transformer);
    }

    /**
     * Returns a temporary Module based on question category
     *
     * @param $category_id
     * @return \Dingo\Api\Http\Response
     *
     */
    public function makeTempModuleByCategory($category_id) {
        $validIds = $this->getUserCareerTrack()->categories->pluck('id')->toArray();
        
        if (!in_array($category_id, $validIds)) {
            return response()->json([
                'error' => 'Category is not in career track'
            ], Response::HTTP_BAD_REQUEST);
        }

        // Create new Module, but don't persist to DB
        $module = Module::CreateFromCategoryId($category_id);

        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Create a new quiz Module
     *
     * @param \Illuminate\Http\Request $request
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function createModule(Request $request) {
        $form_data = $request->all();
        // Validate form input
        $validator = Validator::make($form_data, [
            "mode"                  => 'required|string|in:timed,untimed,tutor,review',
            "question_types"        => 'required',
            "categories"            => 'required',
            "max_allowed_questions" => 'required|int',
            "display_name"          => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'form_errors_could_not_create_question_module',
                'form_errors' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $question_types = $request->question_types;

        // Build query to get questions based on request
        $question_query = QBankQuestion::select('id')->where('status', 'Approved');

        // Error out if all question types are false
        if (!$question_types['all'] && !$question_types['bookmarked'] && !$question_types['incorrect'] && !$question_types['unused']) {
            return response()->json([
                'error' => 'must_specify_at_least_one_question_type',
            ], Response::HTTP_BAD_REQUEST);
        }

        // Check if review mode that we are only looking at incorrect questions
        if ($form_data['mode'] == 'review' && $question_types['incorrect'] &&
            ($question_types['all'] || $question_types['bookmarked'] || $question_types['unused'])) {
            return response()->json([
                'error' => 'review_modules_must_contain_only_incorrect',
            ], Response::HTTP_BAD_REQUEST);
        }

        // Narrow down question query by question types
        $type = 'all';

        if(!$question_types['all']) {
            // include questions that have been bookmarked
            if ($question_types['bookmarked']) {
                $bookmarked_question_ids = $user->markedqbanks()->select('id')->get()->pluck('id')->toArray();
                $question_query = $question_query->whereIn('id', $bookmarked_question_ids);
                $type = 'bookmarked';
            }
            // include questions that have been answered incorrectly
            elseif ($question_types['incorrect']) {
                $wrong_question_ids = QBankResult::select('qbank_id')->where('user_id', $user->id)->where('result', QBankResult::WRONG)->get()->pluck('qbank_id')->toArray();
                $question_query = $question_query->whereIn('id', $wrong_question_ids);
                $type = 'incorrect';
            }
            // exclude questions that have been attempted
            elseif ($question_types['unused']) {
                $attempted_question_ids = QBankResult::select('qbank_id')->where('user_id', $user->id)->get()->pluck('qbank_id')->toArray();
                $question_query = $question_query->whereNotIn('id', $attempted_question_ids);
                $type = 'unused';
            }
        }

        if ($request->categories !== 'all') {
            $categories = $request->categories;
            $question_query = $question_query->whereIn('category_id', $categories);
        }

        $questions = $question_query
            ->take($request->max_allowed_questions)
            ->orderByRaw("RAND()")
            ->get()
            ->pluck('id')
            ->toArray();

        if (count($questions) < 1) {
            return response()->json([
                'error' => 'no_questions_found_matching_parameters',
            ], Response::HTTP_BAD_REQUEST);
        }

        $timer_minutes = null;
        $num_questions = count($questions);
        if ($request->mode === 'timed' && $num_questions > 0) {
            $seconds = $num_questions * Module::TIMED_TEST_SECONDS_PER_QUESTION;
            $timer_minutes = floor($seconds / 60);
        }

        try {
            $module = Module::create([
                'settings->display_name'    => $request->display_name,
                'settings->active'          => true,
                'user_id'                   => $this->getUserId(),
                'testmode'                  => $request->mode,
                'questionmode'              => $type,
                'questionids'               => implode($questions, ','),
                "timerlength"               => $timer_minutes
            ]);
        } catch (JWTException $e) {
            \Bugsnag::notifyException($e);
            return response()->json(['error' => 'could_not_create_module'], 500);
        }

        if ($form_data['mode'] == 'review') {
            $global_results = $module->getGlobalResults($this->getUserId())->where('answer', '>', 0);
            foreach ($global_results as $result) {
                $module->addAnswerAttempt($result->qbank_id, $result->answer, $result->is_correct);
            }
        }

        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Update a quiz Module
     *
     * @param \Illuminate\Http\Request $request
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function updateModule(Request $request) {

        // Handle inputs
        $id = $request->module_id;
        $form_data = $request->all();

        $user    = app('Dingo\Api\Auth\Auth')->user();
        $user    = User::findOrFail($user->id);

        $module = Module::findOrFail($id);

        // Validate form input
        $validator = Validator::make($form_data, [
            "testmode"      => 'string',
            "questionmode"  => 'string',
            "questionids"   => 'string',
            "starttime"     => 'int',
            "timerlength"   => 'int',
            "numcorrect"    => 'int',
            "finishtime"    => 'int',
            'display_name'  => 'string',
            'active'        => 'boolean',
        ]);

        // Quit if validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => 'form_errors_could_not_create_question_module',
                'form_errors' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // Save updates to DB
        $module->fill($form_data)->save();

        // Save display name to DB
        if (isset($request->display_name)) {
            $module->update(['settings->display_name' => $request->display_name]);
        }

        // Save active status to DB
        if (isset($request->active)) {
            $module->update(['settings->active' => $request->active]);
        }

        // Handle response
        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Sets Active field to false for a given module
     *
     * @param \Illuminate\Http\Request $request
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function deleteModule($module_id) {

        // Handle inputs
        $module = Module::findOrFail($module_id);

        // Save updates to DB
        $module->update(['settings->active'=>false]);

        // Handle response
        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Starts a Module setting starttime
     *
     * @param \Illuminate\Http\Request $request
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function moduleStarted(Request $request) {
        // Handle inputs
        $form_data = $request->all();

        $module = Module::findOrFail($request->module_id);

        // Quit if already complete or started
        if ($module->complete) {
            return response()->json([
                'error' => 'module_has_already_been_completed',
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($module->starttime) {
            return response()->json([
                'error' => 'module_has_already_been_started',
            ], Response::HTTP_BAD_REQUEST);
        }

        // Validate form input
        $validator = Validator::make($form_data, [
            "starttime"    => 'int',
        ]);

        // Quit if validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => 'form_errors_start_time_must_be_epoch_integer',
            ], Response::HTTP_BAD_REQUEST);
        }

        // If finish time is empty, set it to the current epoch time
        if (!isset($form_data['starttime'])) {
            $form_data['starttime'] = time();
        }

        // Save updates to DB
        $module->update([
            'starttime'    => $form_data['starttime'],
        ]);

        // Handle response
        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Finish out a given Module
     *
     * @param \Illuminate\Http\Request $request
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function moduleFinished(Request $request) {
        $module = Module::findOrFail($request->module_id);

        // Quit if already complete
        if ($module->complete) {
            return response()->json([
                'error' => 'module_has_already_been_completed',
            ], Response::HTTP_BAD_REQUEST);
        }

        $finishtime = $request->finishtime ?? time();
        if (!(string)(int)$finishtime === $finishtime) {
            return response()->json([
                'error' => 'form_errors_finish_time_must_be_epoch_integer',
            ], Response::HTTP_BAD_REQUEST);
        }
        if (!$module->starttime) {
            return response()->json([
                'error' => 'form_errors_module_has_not_been_started',
            ], Response::HTTP_BAD_REQUEST);
        }
        if ((int)$finishtime < $module->starttime) {
            return response()->json([
                'error' => 'form_errors_finish_time_must_be_greater_than_start_time',
            ], Response::HTTP_BAD_REQUEST);
        }

        $module->update([
            'finishtime' => $finishtime,
            'complete'   => 1,
            'numcorrect' => $module->numcorrect,
        ]);

        $transformer = new ModuleTransformer();
        $transformer->noQuestionIdsShuffle();

        return $this->response->item($module, $transformer);
    }

    /**
     * Retake a quiz Module
     *
     * @return \Dingo\Api\Http\Response|\Illuminate\Http\JsonResponse
     *
     */
    public function retakeModule($module_id) {
        // Find module
        $module = Module::findOrFail($module_id);

        // Save updates to DB
        $module->update([
            'complete'        => 0,
            'finishtime'      => null,
            'starttime'       => null,
            'answer_attempts' => null,
        ]);

        // Handle response
        return $this->response->item($module, new ModuleTransformer);
    }
}

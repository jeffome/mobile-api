<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Transformers\TopicHistoryTransformer;
use App\Transformers\TopicStudiedTransformer;
use App\Http\Resources\Collection;
use App\User;
use App\UserMetric;
use App\TopicHistory;
use Config;
use DB;
use Validator;


class MetricController extends Controller
{
    /**
     * @TODO refactor this method
     * Responds to requests to GET /metrics/recent-topics
     */
    public function getRecentTopics(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        // because of the groupby query and raw SQL, we must do the pagination manually
        $limit = $request->input('limit', Config::get('pagination.itemsPerPage'));
        $page  = $request->input('page', 1);
        $skip  = ($page - 1) * $limit;

        $topicIdsInTrack = $user->careerTrack->getTopics()->pluck('id')->toArray();
        $topicIdsInTrackStr = implode(",", $topicIdsInTrack);

        // this query fetches distinct topic_id along with all other attributes of the most recent record for each topic
        // http://stackoverflow.com/questions/612231/how-can-i-select-rows-with-maxcolumn-value-distinct-by-another-column-in-sql
        $sql = "
            FROM topic_history th
            INNER JOIN
                (SELECT DISTINCT topic_id, MAX(time) AS MaxDateTime, user_id
                FROM topic_history
                WHERE user_id = ?
                GROUP BY topic_id) groupedth
            ON th.topic_id = groupedth.topic_id
            AND th.time = groupedth.MaxDateTime
            AND th.user_id = groupedth.user_id
            AND th.topic_id in ($topicIdsInTrackStr)
        ";

        // get total unpaged
        $count        = 0;
        $totalPages   = 0;
        
        $totalRecords = DB::select('SELECT COUNT(th.id) as total' . $sql . ';', [$user->id]);
        if (is_array($totalRecords)) {
            $totalRecords = current($totalRecords);
            if (property_exists($totalRecords, 'total')) {
                $count = (int) $totalRecords->total;
            }
        }
        if ($count > 0 && $limit > 0) {
            $totalPages = ceil($count / $limit);
        }

        // get paged results
        $recentTopics = DB::select('SELECT th.* ' . $sql . ' ORDER BY time DESC, id DESC LIMIT ?,?;', [
            $user->id,
            $skip,
            $limit,
        ]);

        // because of the groupby query and raw SQL, we must do the pagination manually
        $response = [
            'data' => [],
            'meta' => [
                'pagination' => [
                    'total'        => $count, // total records
                    'count'        => count($recentTopics), // total on this page
                    'per_page'     => $limit,
                    'current_page' => $page,
                    'total_pages'  => $totalPages,
                    'links'        => [],
                ],
            ],
        ];

        $transformer = new TopicHistoryTransformer();
        foreach ($recentTopics as $recentTopic) {
            // I hate this... necessary in order to use the transformer
            if ($topicHistoryItem = TopicHistory::find($recentTopic->id)) {
                $response['data'][] = $transformer->transform($topicHistoryItem);
            }
        }

        return response()->json($response);
    }

    /**
     * Responds to requests to POST /metrics/recent-topics/watched/{id}
     */
    public function resetOrCreateRecentTopic($topicId, Request $request) {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $recentTopic = $user->recentTopics()->where('topic_id', $topicId)->first();

        if ($recentTopic && $recentTopic['reviewed_at'] == null) {
            return response()->json([
                'data' => null,
            ], Response::HTTP_OK);
        }

        
        $recentTopic = $user->recentTopics()->create([
            'topic_id' => $topicId,
            'time' => Carbon::now(),
        ]);


        if (!$recentTopic) {
            return response()->json([
                'message' => 'Could not create or find recent topic',
                'error' => null,
            ], Response::HTTP_BAD_REQUEST);
        } 
        
        return $this->response->item($recentTopic, new TopicHistoryTransformer);
    }

    /**
     * Responds to requests to POST /metrics/recent-topics/{id}
     */
    public function postRecentTopics($topicId, Request $request)
    {
        $payload = $request->only('last_synced_at', 'reviewed_at', 'next_due_at', 'next_due_interval_days');

        $validator = Validator::make($payload, [
            'last_synced_at'         => 'date',
            'reviewed_at'            => 'date',
            'next_due_at'            => 'date',
            'next_due_interval_days' => 'integer',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Could not update recent topic',
                'error' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $latestRecord = $user->recentTopics()->where('topic_id', $topicId)->orderBy('time', 'DESC')->orderBy('id', 'DESC')->firstOrFail();

        if (isset($payload['last_synced_at']) && !empty($payload['last_synced_at'])) {
            $latestRecord->last_synced_at = $this->convertUtcToServerLocal($payload['last_synced_at'])->toDateTimeString();
        }

        if (isset($payload['reviewed_at']) && !empty($payload['reviewed_at'])) {
            $latestRecord->reviewed_at = $this->convertUtcToServerLocal($payload['reviewed_at'])->toDateTimeString();
        }

        if (isset($payload['next_due_at']) && !empty($payload['next_due_at'])) {
            $latestRecord->next_due_at = $this->convertUtcToServerLocal($payload['next_due_at'])->toDateTimeString();
        }

        if (isset($payload['next_due_interval_days'])) {
            $latestRecord->next_due_interval_days = $payload['next_due_interval_days'];
        }

        $latestRecord->save();

        return $this->response->item($latestRecord, new TopicHistoryTransformer);
        
    }

    /**
     * Responds to requests to GET /metrics/studied-topics
     */
    public function studiedTopics(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $topicIdsInTrack = $user->careerTrack->getTopics()->pluck('id');
        $studiedTopics = $user->studiedTopics()
            ->whereIn('topic_id', $topicIdsInTrack)
            ->select(DB::raw('*, max(created_at) as created_at'))
            ->groupBy('topic_id')
            ->orderBy('created_at', 'DESC')
            ->get();

        return new Collection($studiedTopics, new TopicStudiedTransformer);
    }

    /**
     * Creates or updates new user metrics
     */
    public function createOrUpdateMetric(Request $request) {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $payload = $request->only('type', 'object_id', 'description');

        $validator = Validator::make($payload, [
            'type'         => 'required|string',
            'object_id'    => 'required|integer',
            'description'  => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'bad_metric_params',
                'form_errors' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }           

        $metric = UserMetric::updateOrCreate(
            [
                'user_id' => $user->id,
                'type' => $payload['type'],
                'object_id' => $payload['object_id'],
                'is_remediation' => false
            ],
            [
                'ip' => $request->ip(),
                'description' => $payload['description'],
                'updated_at' => Carbon::now(),
            ]
        );

        $response = [
            'data' => $metric, 
        ];
        
        return response()->json($response);        
    }
}

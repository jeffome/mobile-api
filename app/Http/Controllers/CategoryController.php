<?php

namespace App\Http\Controllers;

use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
    * Responds to requests to GET /categoriesWithStats
    */
    public function withStats(Request $request)
    {
        $categories = $this->getUserCareerTrack()
                           ->categories()
                           ->where('is_active_flashback', 1)
                           ->statsData()
                           ->get();

        // @TODO: refactor the code below to not have "manually built response for this asset"
        $data = [];
        $transformer = new CategoryTransformer();
        foreach ($categories as $key=>$category)
        {
            $videos_duration = 0;
            foreach ($category->topics as $topic)
            {
                $videos_duration += $topic->duration;
            }

            $category->videos_duration = ((int)($videos_duration / 60));
            $data[$key] = $transformer->transform($category);
        }
        
        return response()->json([ 'data' => $data ]);
    }

}

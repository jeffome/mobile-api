<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

use App\Track;
use App\User;
use Customer;
use Product;

class ProductsController extends Controller
{
    private function _hasInternBootcamp($products)
    {
        return count(array_filter((array) $products, function ($product) {
                return $product->product->sku === "intern_bootcamp";
            })) > 0;
    }

    private function _hasFlashback($products)
    {
        return count(array_filter((array) $products, function ($product) {
                return $product->product->sku === "flashback_access";
            })) > 0;
    }

    private function _getTopics($products)
    {
        return array_values(array_map(function ($product) {
            return intval(trim($product->product->sku, "mobile_lesson_"));
        }, array_filter((array) $products, function ($product) {
            return $product->product->type === "topic_access";
        })));
    }

    private function _getBooks($products)
    {
        return array_values(array_map(function ($product) {
            return $product->product->sku;
        }, array_filter((array) $products, function ($product) {
            return $product->product->type === "book";
        })));
    }

    private function _getSubscriptions($subscriptions)
    {
        $tracks = \Cache::remember(__METHOD__ . '::subscribable-tracks', Carbon::MINUTES_PER_HOUR, function () { return Track::where('is_subscribable', 1)->get(); });
        return $tracks->keyBy('slug')->map(function($track) use ($subscriptions) {
            return collect($subscriptions)->where('client_track_id', $track->id)->where('status', 'active')->count() > 0;
        });
    }

    public function getProducts(Request $Request)
    {
        $products = \Cache::remember(
            __METHOD__,
            Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY,
            function () {
                return Product::get();
            }
        );

        return response()->json([
            "data" => [
                "success" => $products["ok"] ? true : false,
                "products" => $products["response"]
            ]
        ]);
    }

    public function getProductAccess(Request $Request)
    {
        $customer = \Cache::remember(
            __METHOD__ . '::user:' . $this->getUser()->id . '::customer',
            5,
            function () {
                return Customer::getOrCreate($this->getUser());
            }
        );

        if (!$customer || !isset($customer->subscription)) {
            return response()->json([
                'success' => false,
                'message' => 'Could not get customer information',
                'error' => null,
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $purchases = \Cache::remember(
            __METHOD__ . '::user:' . $this->getUser()->id . '::purchases',
            5,
            function () {
                return Customer::getPurchases($this->getUser());
            }
        );

        if (!$purchases || !isset($purchases->products)) {
            return response()->json([
                'success' => false,
                'message' => 'Could not get customer purchases information',
                'error' => null,
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $authUser = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($authUser->id);
        $freePremium = $user->hasFreePremium();

        $subscriptions = collect($this->_getSubscriptions($customer->subscription));
        $subscriptions = $subscriptions->map(function ($sub) use ($freePremium) {
            return $freePremium || $sub;
        });

        return response()->json([
            "data" => [
                "success" => true,
                "access" => [
                    'flashback' => $this->_hasFlashback($purchases->products),
                    'intern_bootcamp' => $this->_hasInternBootcamp($purchases->products),
                    'topics' => $this->_getTopics($purchases->products),
                    'books' => $this->_getBooks($purchases->products),
                    'subscriptions' => $subscriptions,
                ]
            ]
        ]);
    }
}

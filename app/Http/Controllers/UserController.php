<?php

namespace App\Http\Controllers;

use App\Social;
use App\Transformers\UserTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Track;
use Validator;

class UserController extends Controller
{
    /**
     * Allows setting a trial end date for the user
     */
    public function setTrialEndDate(Request $request)
    {
        $timestamp = strtotime($request->date);

        if ($request->date && $timestamp !== false) {
            $formattedDate = date('Y-m-d H:i:s', $timestamp);

            $user = app('Dingo\Api\Auth\Auth')->user();
            $user = User::findOrFail($user->id);

            $user->flashback_trial_end_date = $formattedDate;
            $user->save();

            return $this->response->item($user, new UserTransformer);
        }

        return response()->json([
            'error' => 'Invalid Date',
        ]);
    }

    /**
     * Creates new social link for users
     */
    public function linkSocial(Request $request)
    {
        /** @var \Illuminate\Validation\Validator $validator */
        $validator = Validator::make($request->only('uid', 'provider', 'oauth2_token', 'profile_photo_url'), [
            'uid' => 'required',
            'provider' => 'required',
            'oauth2_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], 400);
        }

        $params             = $request->only('uid', 'provider', 'oauth2_token', 'profile_photo_url');
        $params['provider'] = strtolower($params['provider']);
        $supportedProviders = ['facebook', 'google'];

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        // We've already validated the user, so let's change the
        // profile pic even if the social link below fails
        if (!empty($params['profile_photo_url']) && $user->imageurl == null) {
            $user->imageurl = $params['profile_photo_url'];
            $user->save();
        }

        // check provider
        if (!in_array($params['provider'], $supportedProviders)) {
            return response()->json([
                'error' => 'Provider not supported',
            ], 400);
        }
        
        // check if the there is another account associated
        $social = Social::where('uid', $params['uid'])
            ->where('provider', $params['provider'])
            ->first();

        if ($social) {
            return response()->json([
                'error' => 'There is an account already linked with that ' . ucwords($params['provider']) . ' user',
            ], 401);
        }

        // check if user has already linked the account
        $social = Social::where('user_id', $user->id)
            ->where('provider', $params['provider'])
            ->first();

        if ($social) {
            return response()->json([
                'error' => 'User is already linked with ' . ucwords($params['provider']),
            ], 400);            
        }
        
        // create social link
        $social = Social::create([
            'user_id'             => $user->id,
            'uid'                 => $params['uid'],
            'provider'            => $params['provider'],
            'oauth2_access_token' => $params['oauth2_token'],
            'oauth2_expires'      => Carbon::now()->addMonths(2),
        ]);

        if ($social) {
            return response()->json([
                'success' => 'New link created with ' . ucwords($params['provider']),
            ], 200);
        } else {
            return response()->json([
                'error' => 'There was an error creating a new social link. Please try again later.',
            ], 500);
        }
    }

    /**
     * Removes social link for users
     */
    public function unLinkSocial(Request $request)
    {
        $validator = Validator::make($request->only('provider'), [
            'provider' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing params',
                'error' => $validator->errors(),
            ], 400);
        }
        
        $params             = $request->only('provider');
        $params['provider'] = strtolower($params['provider']);
        $supportedProviders = ['facebook', 'google'];

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        if (!in_array($params['provider'], $supportedProviders)) {
            return response()->json([
                'error' => 'Provider not supported',
            ], 400);
        }
        

        $rows = Social::where('user_id', $user->id)
            ->where('provider', $params['provider'])
            ->delete();

        if ($rows == 0) {
            return response()->json([
                'error' => 'User is not linked with ' . ucwords($params['provider']),
            ], 400);
        } else {
            return response()->json([
                'success' => 'Link with ' . ucwords($params['provider']) . ' removed',
            ], 200);
        }
    }

    /**
     * Set Avatar
     */
    public function setAvatar(Request $request)
    {
        $params = $request->only('uid', 'provider', 'oauth2_token', 'profile_photo_url');

        if (!empty($params['profile_photo_url'])) {
            $user           = app('Dingo\Api\Auth\Auth')->user();
            $user           = User::findOrFail($user->id);
            $user->imageurl = $params['profile_photo_url'];
            $user->save();
            return $this->response->item($user, new UserTransformer);
        } else {
            return response()->json([
                'error' => 'Please provide a profile photo URL',
            ]);
        }
    }

    /**
     * Set Avatar
     */
    public function setCareerTrack(Request $request)
    {
        $params = $request->only('career_track_id');
        if (!empty($params['career_track_id'])) {

            $count = Track::where('id','=',$params['career_track_id'])->count();
            if ($count < 1) {
                return response()->json([
                    'error' => 'Invalid career track',
                ]);
            }


            $user           = app('Dingo\Api\Auth\Auth')->user();
            $user           = User::findOrFail($user->id);
            $user->career_track_id = $params['career_track_id'];
            $user->save();
            return $this->response->item($user, new UserTransformer);
        } else {
            return response()->json([
                'error' => 'Please provide a career track id',
            ]);
        }
    }
}

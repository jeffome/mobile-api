<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CacheController extends Controller
{
    public function getLastUpdated(Request $Request) // (DEPRECATED)
    {
        $topic = DB::table('topics')->orderBy('updated_at', 'desc')->first();
        $category = DB::table('categories')->orderBy('updated_at', 'desc')->first();
        $clerkship = DB::table('clerkships')->orderBy('updated_at', 'desc')->first();
        $flashcard = DB::table('flashcards')->orderBy('updated_at', 'desc')->first();

        return response()->json([
            "data" => [
                "lastUpdated" => [
                    "categories" => $category->updated_at,
                    "clerkships" => $clerkship->updated_at,
                    "flashcards" => $flashcard->updated_at,
                    "topics" => $topic->updated_at,
                ]
            ]
        ]);
    }

}

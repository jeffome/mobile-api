<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Transformers\UserSettingTransformer;
use App\User;
use App\UserSetting;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Validator;

class UserSettingController extends Controller
{
    /**
     * Responds to requests to POST /users/settings
     */
    public function setSettings(Request $request)
    {
        $user             = app('Dingo\Api\Auth\Auth')->user();
        $user             = User::findOrFail($user->id);
        $validBooleanKeys = [
            'include_step_2' => 'boolean',
            'include_step_3' => 'boolean',
            'timer_7_day'    => 'boolean',
            'timer_14_day'   => 'boolean',
            'timer_30_day'   => 'boolean',
        ];

        $payload = $request->only(array_merge(array_keys($validBooleanKeys), ['updated_at']));

        $validator = Validator::make($payload, $validBooleanKeys);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not store user settings.', $validator->errors());
        } else {
            // null is an invalid value, so force false if missing
            foreach ($validBooleanKeys as $key => $vlue) {
                if (!isset($payload[$key])) {
                    $payload[$key] = false;
                }
            }

            $setting = UserSetting::where('user_id', $user->id)->first();

            if ($setting) {
                $setting->include_step_2 = $payload['include_step_2'];
                $setting->include_step_3 = $payload['include_step_3'];
                $setting->timer_7_day    = $payload['timer_7_day'];
                $setting->timer_14_day   = $payload['timer_14_day'];
                $setting->timer_30_day   = $payload['timer_30_day'];
                $setting->save();
            } else {
                $setting = UserSetting::create([
                    'user_id'        => $user->id,
                    'include_step_2' => $payload['include_step_2'],
                    'include_step_3' => $payload['include_step_3'],
                    'timer_7_day'    => $payload['timer_7_day'],
                    'timer_14_day'   => $payload['timer_14_day'],
                    'timer_30_day'   => $payload['timer_30_day'],
                ]);
            }

            // allow overriding the created at field
            if (isset($payload['updated_at'])) {
                $updated_at          = $this->convertUtcToServerLocal($payload['updated_at']);
                $setting->updated_at = $updated_at->toDateTimeString();
                $setting->save();
            }

            return $this->response->item($setting, new UserSettingTransformer);
        }
    }

    /**
     * Responds to requests to GET /users/settings
     */
    public function getSettings(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        // creates settings with default values if missing
        $setting = $user->getSettings();

        return $this->response->item($setting, new UserSettingTransformer);
    }
}

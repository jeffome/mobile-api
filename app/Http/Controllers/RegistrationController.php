<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Reminder;
use App\User;
use App\UserProfession;
use App\Transformers\UserTransformer;
use Validator;
use JWTAuth;

class RegistrationController extends Controller
{

    protected $reminder;

    /**
     * Create a new controller instance.
     *
     * @param \App\Reminder $reminder
     */
    public function __construct(Reminder $reminder)
    {
        $this->reminder = $reminder;

    }

    /**
     * Validate signup from email form and update to DB
     *
     * @param  Request $request input from sign up form
     * @return string json response
     *
     */
    public function signUp(Request $request) {
        // TODO: send email for email validation
        $params = $request->only(
            'user_type',
            'profession_id',
            'school_text_id',
            'year',
            'first_name',
            'last_name',
            'email',
            'password',
            'password_confirmation'
        );

        $validator = Validator::make($params, [
            'first_name'            =>  'required|string',
            'last_name'             =>  'required|string',
            'email'                 =>  'required|email',
            'password'              =>  'required',
            'password_confirmation' =>  'required|same:password',
            'user_type'             =>  'required|in:professional,student',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing information. Could not register user',
                'error' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($params['password'] != $params['password_confirmation']) {
            return response()->json([
                'message' => 'Password does not match.',
                'error' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);  
        }

        if ($params['user_type'] == 'student') {
            $validator = Validator::make($params, [
                'school_text_id'        =>  'required|string',
                'year'                  =>  'required|string',
            ]);
        }

        if ($params['user_type'] == 'professional') {
            $validator = Validator::make($params, [
                'profession_id' => 'required|string',
            ]);
        }


        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing information. Could not register user',
                'error' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // Create new user
        $user = User::firstOrCreate(['email' => strtolower($params['email'])]);

        // Return error if user already exists in DB
        if (!$user->wasRecentlyCreated) {
            return response()->json([
                'message' => 'An account has already been registered with this email.',
                'errors' => []
            ], Response::HTTP_BAD_REQUEST);
        }

        $userInfo = [
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'year' => $params['year'],
            'school_text_id' => $params['school_text_id'],
            'password' => $params['password'],  
            'career_track_id' => 2 // defaults to clinical
        ];
     
        // Persist model to DB if new
        $user->fill($userInfo);
        $user->save();

        if ($params['user_type'] == 'professional') {
            $userProfession = UserProfession::firstOrCreate([
                'user_id' => $user->id,
                'profession_id' => $params['profession_id']
            ]);          
        }
        
        // Setup response array
        $userTransformer = new UserTransformer();
        $response = [
            'data' => $user,
            'token' => JWTAuth::fromUser($user),
            'signup_info' => [
                'wrote_to_db' => true,
                'message' => 'new user created'
            ]
        ];

        $user->sendActivationEmail();

        // Return response json
        return response()->json($response);
    }

    /**
     * Validate signup from social media and udpate to db
     *
     * @param  Request $request input from sign up form
     * @return string json response
     *
     */
    public function socialSignup(Request $request)
    {
        // TODO: send email for email validation
        // TODO: send an email to reset user password

        $params = $request->only(
            'provider',
            'uid',
            'oauth2_token',
            'first_name',
            'last_name',
            'email',
            'user_type',
            'school_text_id',
            'year',
            'profession_id'
        );

        $validator = Validator::make($params, [
            'provider'      => 'required',
            'uid'           => 'required',
            'oauth2_token'  => 'required',
            'first_name'    =>  'required|string',
            'last_name'     =>  'required|string',
            'email'         =>  'required|email',
            'user_type'     =>  'required|in:professional,student',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing information. Could not register user',
                'error' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($params['user_type'] == 'student') {
            $validator = Validator::make($params, [
                'school_text_id'    =>  'required|string',
                'year'              =>  'required|string',
            ]);
        }

        if ($params['user_type'] == 'professional') {
            $validator = Validator::make($params, [
                'profession_id' => 'required|string',
            ]);
        }

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Missing information. Could not register user',
                'error' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // check social credentials
        $check = AuthenticateController::checkSocialCredentials($params);
        if (!$check['is_valid']) {
            return response()->json([
                'message' => 'Invalid Credentials',
                'error' => $check,
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = User::firstOrCreate(['email' => strtolower($params['email'])]);

        if (!$user->wasRecentlyCreated) {                
            return response()->json([
                'message' => 'An account has already been registered with this email.',
                'errors' => []
            ], Response::HTTP_BAD_REQUEST);
        }

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $genPassword = '';
        for ($i = 0; $i < 26; $i++) {
            $genPassword .= $characters[rand(0, $charactersLength - 1)];
        }

        $password = Hash::make($genPassword);

        $userInfo = [
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'password' => $password,  
        ];

        if ($params['user_type'] == 'student') {
            $userInfo['year'] = $params['year'];
            $userInfo['school_text_id'] = $params['school_text_id'];
            $userInfo['career_track_id'] = optional($params)['career_track_id'] ?? 2;
        } else {
            $userInfo['year'] = null;
            $userInfo['school_text_id'] = null;
            $userInfo['career_track_id'] = null;
        }
     
        // Persist model to DB if new
        $user->fill($userInfo);
        $user->save();

        if ($params['user_type'] == 'professional') {
            $userProfession = UserProfession::firstOrCreate([
                'user_id' => $user->id,
                'profession_id' => $params['profession_id']
            ]);          
        }

        // Setup response array
        $userTransformer = new UserTransformer();
        $response = [
            'data' => $userTransformer->transform($user),
            'token' => JWTAuth::fromUser($user),
            'signup_info' => [
                'wrote_to_db' => true,
                'message' => 'new user created'
            ]
        ];

        $user->activateWithoutEmail();
        $reminder = $this->reminder->checkIfReminder($user);
        $reminder->emailSocialSetup($params['email']);

        // Return response json
        return response()->json($response);      
    }

    /**
     * Validate password reset form and update db
     *
     * @param  Request $request input from password reset form
     * @return string json response
     *
     */
    public function passwordReset(Request $request)
    {

        $form_data = $request->all();

        // Validate form input
        $validator = Validator::make($form_data, [
            'email'                 =>  'required|email',
            'password'              =>  'required',
            'password_confirmation' =>  'required|same:password',
        ]);

        // Quit if validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => 'could_not_update_password',
                'form_errors' => $validator->errors()
            ], 400);
        }

        try {
            // Search for email in DB
            $data = User::where('email', $form_data['email'])->first();

            if($data && Hash::check($form_data['password'], $data->password)) {
                return response()->json(['error' => 'new_password_identical_to_old_password'], 400); // handle matching passwords
            } elseif ($data) {
                $data->password = $form_data['password'];
                $data->save();
                $message = 'Password successfully updated';
            } else {
                return response()->json(['error' => 'could_not_find_email'], 400); // handle if email is not found
            }
        } catch (JWTException $e) {
            \Bugsnag::notifyException($e);
            // something went wrong whilst attempting to update the password
            return response()->json(['error' => 'could_not_update_password'], 500);
        }

        // Attempt grabbing access token
        $token = JWTAuth::attempt(['email' => $form_data['email'], 'password' => $form_data['password']]);
        
        return response()->json(compact('data','message', 'token'));

    }

    public function resendActivationEmail(Request $request)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user->sendActivationEmail();
        return response()->json(compact(['success'  => true]));
    }

    /**
     * Send password reset email
     *
     * @param  Request $request must contain email
     * @return string json response string
     *
     */
    public function sendPasswordResetEmail(Request $request)
    {

        $emailField = 'email';
        $email = $request->email;

        $validator = Validator::make($request->only('email'), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'could_not_send_email',
                'form_errors' => $validator->errors()
            ], 400);
        }

        $user = User::where('email', $email)->first();

        if (!$user) {
            $user = User::where('alternate_email', $email)->first();
            if ($user) $emailField = 'alternate_email';
        }

        if (!$user) {
             return response()->json([
                 'error' => 'no_user_found_for_given_email',
             ], 400);
        }

        if($user->is_banned) {
            return response()->json([
                'error' => 'user_is_banned',
            ], 400);
        }

        try {
            $reminder = $this->reminder->checkIfReminder($user);

            return response()->json([
                'message' => $reminder->emailReminder($email),
            ]);
        } catch (JWTException $e) {
            \Bugsnag::notifyException($e);
            // something went wrong whilst attempting to update the password
            return response()->json(['error' => 'could_not_send_password_reset_email'], 500);
        }

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Transformers\SystemDeckTransformer;
use App\Flashcard;
use App\SystemDeck;
use App\SystemDeckType;
use App\User;
use Validator;

class SystemDeckController extends Controller
{
    /**
     * GET /system-decks
     */
    public function index()
    {
        $decksExist = false;
        $deckTypeIds = SystemDeckType::where('is_configurable', true)->get()->pluck('id')->toArray();
        foreach ($deckTypeIds as $id) {
            $deckExist = $this->getUser()->doesSystemDeckExistForUser($id); // ensure the system deck actually exists
            $decksExist = $decksExist || $deckExist;
        }

        if (!$decksExist) {
            return response()->json([
                'message' => 'Could not find system decks',
            ], Response::HTTP_BAD_REQUEST);
        }
        // Return api response
        return $this->response->collection($this->getUser()->systemDecks, new SystemDeckTransformer());
    }

    /**
     * GET /system-decks/{systemDeckTypeId}
     */
    public function show($systemDeckTypeId)
    {
        $deck = $this->getUser()->getSystemDeckByTypeId($systemDeckTypeId);
        if ($deck) {
            return $this->response->item($deck, new SystemDeckTransformer());
        }

        // create the user's requested system deck if it doesn't exist
        // only configurable system decks can be created at request time
        $systemDeckType = SystemDeckType::where('is_configurable', 1)->find($systemDeckTypeId);
        if (!$systemDeckType) {
            return response()->json([
                'message' => 'Could not create system deck type',
                'error' => 'system_deck_type_invalid'
            ], Response::HTTP_BAD_REQUEST);
        }

        $deck = SystemDeck::create([
            'user_id'                       => $this->getUserId(),
            'flashcard_system_deck_type_id' => $systemDeckTypeId,
        ]);
        return $this->response->item($deck, new SystemDeckTransformer());
    }

    /**
     * Responds to requests to PUT /system-decks/<system_deck_type_id>
     */
    public function updateSystemDecks($systemDeckTypeId, Request $request)
    {
        $payload = $request->only('flashcard_ids', 'updated_at');
        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $validator = Validator::make($payload, [
            'flashcard_ids' => 'array',
            'updated_at' => 'date'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Could not update system deck',
                'error' => $validator->errors(),
            ], Response::HTTP_BAD_REQUEST);
        }

        if (empty($payload['flashcard_ids'])) {
            $payload['flashcard_ids'] = [];
        }

        $deck = $user->getSystemDeckByTypeId($systemDeckTypeId);
        if ($deck === null) {
            return response()->json([
                'message' => 'Could not find system deck',
                'error' => 'system_deck_not_found'
            ], Response::HTTP_NOT_FOUND);
        }

        if (isset($payload['updated_at'])) {
            $updated_at = $this->convertUtcToServerLocal($payload['updated_at']);
            $deck->updated_at = $updated_at->toDateTimeString();
            $deck->save();
        } else {
            $deck->updated_at = Carbon::now();
            $deck->save();
        }

        // get all the deck flashcards that don't belong to this career track;
        $trackFlashcards = $deck->flashcardsByTrack()->get();
        $deck->flashcards()->detach($trackFlashcards);
                
        // remove repeated ids if any
        $flashcardIds = array_unique($payload['flashcard_ids']);
        // Ensure that the flashcard ids reference real flashcards
        $updatedTrackFlashcards = Flashcard::whereIn('id', $flashcardIds)->get();

        // attach updated track flashcards to total flashcards
        $deck->flashcards()->attach($updatedTrackFlashcards);

        return $this->response->item($deck, new SystemDeckTransformer);
    }
}

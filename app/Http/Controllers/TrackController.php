<?php

namespace App\Http\Controllers;

use App\Track;
use App\Http\Controllers\Controller;
use App\Http\Resources\Collection;
use App\Transformers\TrackTransformer;
use Illuminate\Http\Request;
use Config;


class TrackController extends Controller
{
    /**
    * Responds to requests to GET /tracks
    */
    public function getTracks(Request $request)
    {
        $tracks = Track::get();
        return new Collection($tracks, new TrackTransformer);
    }

};

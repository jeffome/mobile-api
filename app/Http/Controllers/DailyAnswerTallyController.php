<?php

namespace App\Http\Controllers;

use App\DailyAnswerTally;
use App\Transformers\DailyAnswerTallyTransformer;
use App\User;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Validator;

class DailyAnswerTallyController extends Controller
{
    private function makeAnswerTallyForDay($dateString = null)
    {
        $user = $this->getUser();
        if (is_null($dateString)) {
            $dateString = date('Y-m-d');
        }
        return DailyAnswerTally::firstOrCreate([
            'flipped_on' => $dateString,
            'user_id'    => $user->id,
            'track_id'   => $user->careerTrack->id
        ]);
    }

    public function getTodaysAnswers(Request $request)
    {
        $flippedOn = $this->getUserLocalizedTodayFormatted($request->utc_offset_minutes);
        $tally = $this->makeAnswerTallyForDay($flippedOn);
        return $this->response->item($tally, new DailyAnswerTallyTransformer);
    }

    public function incrementDailyAnswers(Request $request)
    {
        $payload     = $request->only('delta', 'utc_offset_minutes');

        $validator = Validator::make($payload, [
            'delta'              => 'required|integer|min:0',
            'utc_offset_minutes' => 'required|integer',
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not update answer tally.', $validator->errors());
        } else {
            $user = app('Dingo\Api\Auth\Auth')->user();
            $user = User::findOrFail($user->id);

            // localize what "today" means
            $flippedOn = $this->getUserLocalizedTodayFormatted($payload['utc_offset_minutes']);

            $tally = $this->makeAnswerTallyForDay($flippedOn);
            $tally->utc_offset_minutes = $payload['utc_offset_minutes'];
            $tally->total += (int) $payload['delta'];
            $tally->save();

            return $this->response->item($tally, new DailyAnswerTallyTransformer);
        }
    }

    public function setAlertShownAt(Request $request)
    {
        $payload = $request->only('alert_shown_at');

        $validator = Validator::make($payload, [
            'alert_shown_at' => 'required|date',
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not update answer tally.', $validator->errors());
        } else {
            $user = app('Dingo\Api\Auth\Auth')->user();
            $user = User::findOrFail($user->id);

            $alertShownAt = $this->convertUtcToServerLocal($payload['alert_shown_at']);

            $tally                  = $this->makeAnswerTallyForDay($alertShownAt->toDateString());
            $tally->has_shown_alert = true;
            $tally->alert_shown_at  = $alertShownAt->toDateTimeString();
            $tally->save();

            return response()->json([
                'success' => 'alert_shown_at date saved',
            ]);
        }
    }
}

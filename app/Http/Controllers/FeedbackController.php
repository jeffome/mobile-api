<?php

namespace App\Http\Controllers;

use App\Feedback;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Validator;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        $payload = $request->only('feedback');

        $validator = Validator::make($payload, [
            'feedback' => 'required',
        ]);

        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not save new feedback.', $validator->errors());
        } else {

            $user = app('Dingo\Api\Auth\Auth')->user();

            $feedback = Feedback::create([
                'user_id'  => $user->id,
                'feedback' => $payload['feedback'],
            ]);

            $response = [
                'message' => 'Feedback was saved successfully',
            ];

            return $response;
        }
    }
}

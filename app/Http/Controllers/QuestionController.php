<?php

namespace App\Http\Controllers;

use App\Module;
use App\QBankQuestion;
use App\QBankResult;
use App\Transformers\ResultTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class QuestionController extends Controller
{
    /**
    * Responds to requests to PUT /qbank/bookmark/question/{question_id}
    */
    public function bookmarkQuestion(Request $request) {

        $form_data = $request->all();
        // Validate form input
        $validator = Validator::make($form_data, [
            "bookmark" => 'required|boolean',
        ]);

        // Quit if validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => 'form_errors_missing_bookmarked_boolean',
                'form_errors' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // Find the user
        $user = $this->getUser();
        // Find the question and existing bookmarks
        $question = QBankQuestion::findOrFail($request->question_id);
        $bookmarked_questions = $user->markedQbanks()->get();
        
        error_log(json_encode($bookmarked_questions));

        try {
           if ($request->bookmark) {
               $user->markedQbanks()->syncWithoutDetaching($question->id);
           } else {
               $user->markedQbanks()->detach($question->id);
           }
        } catch (JWTException $e) {
            \Bugsnag::notifyException($e);
            // something went wrong whilst attempting to create the bookmark
            return response()->json(['error' => 'could_not_create_bookmark'], 500);
        }

        $message = $request->bookmark ? 'created' : 'removed';
        return response()->json(['success' => 'bookmark_' . $message]);
    }

    /**
    * Responds to requests to PUT /qbank/question/{question_id}/answered
    */
    public function answerQuestion(Request $request) {

        $validator = Validator::make($request->only('answer'), [
            "answer" => 'required|int',
            "module_id" => 'int',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'form_errors_missing_answer',
                'form_errors' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!($request->answer > 0)) {
            return response()->json([
                'error' => 'answer_must_be_an_int_greater_than_zero',
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = app('Dingo\Api\Auth\Auth')->user();
        $user = User::findOrFail($user->id);

        $question = QBankQuestion::findOrFail($request->question_id);

        $result = QBankResult::updateOrCreate([
            "user_id" => $user->id,
            "qbank_id" => $question->id,
        ], [
            "answer"        => $request->answer,
            'result' => $question->correctanswer == $request->answer ? 'C' : 'W',
        ]);

        // If module id is passed in, update the module answer attempts
        if ($request->module_id) {
            $module = Module::findOrFail($request->module_id);
            $answer_attempts = json_decode($module->answer_attempts, true);

            $answer_attempts[$question->id] = [
                "answer" => $result->answer,
                "is_correct" => +$result->is_correct,
            ];

            $module->update(['answer_attempts' => json_encode($answer_attempts)]);
        }

        return $this->response->item($result, new ResultTransformer);
    }

}

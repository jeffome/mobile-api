<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\SystemDeckType;
use App\Transformers\SystemDeckTypeTransformer;
use Config;
use Illuminate\Http\Request;

class SystemDeckTypeController extends Controller
{
    /**
     * Responds to requests to GET /system-deck-types (DEPRECATED)
     */
    public function index(Request $request)
    {
        $itemsPerPage = $request->input('limit', Config::get('pagination.itemsPerPage'));

        $decks = SystemDeckType::paginate($itemsPerPage);

        return $this->response->paginator($decks, new SystemDeckTypeTransformer);
    }

    /**
     * Responds to requests to GET /system-deck-types/1 (DEPRECATED)
     */
    public function show($id)
    {
        $decks = SystemDeckType::findOrFail($id);
        return $this->response->item($decks, new SystemDeckTypeTransformer);
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Flashcard;

class FlashcardController extends Controller
{
    /**
     * Responds to requests to GET /fashcards
     */
    public function index(Request $request)
	{
        $flashcards = \Cache::remember(
        	__METHOD__ . '::track:' . $this->getUserCareerTrack()->id,
			Carbon::MINUTES_PER_HOUR,
			function () {
        		return [
					'data' => $this->getUserCareerTrack()->getFlashcards()->get()
						->map(function(Flashcard $flashcard) {
							return [
								'id'           => (int) $flashcard->getAttributeValue('id'),
								'question'     => $flashcard->getAttributeValue('question'),
								'answer'       => $flashcard->getAttributeValue('answer'),
								'imageurl'     => $flashcard->getAttributeValue('imageurl'),
								'source'       => $flashcard->getAttributeValue('source'),
								'step'         => (int) $flashcard->getAttributeValue('step'),
								'category_id'  => (int) $flashcard->getAttributeValue('category_id'),
								'topic_id'     => (int) $flashcard->getAttributeValue('topic_id'),
								'clerkship_id' => (int) $flashcard->getAttributeValue('clerkship_id'),
								'is_active'    =>  $flashcard->is_active_flashback == 1,
							];
						})
				];
			}
		);

        return response()->json($flashcards);
    }

}

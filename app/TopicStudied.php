<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicStudied extends Model
{
    protected $table = 'topic_studied';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }
}

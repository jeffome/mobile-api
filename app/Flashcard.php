<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flashcard extends Model
{
    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function clerkship()
    {
        return $this->belongsTo('App\Clerkship');
    }

    public function customDecks()
    {
        return $this->belongsToMany('App\CustomDeck', 'flashcard_custom_deck_flashcards', 'flashcard_id', 'flashcard_custom_deck_id')->withTimestamps();
    }

    public function systemDecks()
    {
        return $this->belongsToMany('App\SystemDeck', 'flashcard_system_deck_flashcards', 'flashcard_id', 'flashcard_system_deck_id')->withTimestamps();
    }

    public function answers()
    {
        return $this->hasMany('App\UserAnswer');
    }
}

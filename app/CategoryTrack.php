<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTrack extends Model
{
    protected $table = 'category_track';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'track_id', 'category_id', 'sort'];

    public function flashcards()
    {
        return $this->hasManyThrough('App\Flashcard',
                                     'App\Category',
                                     'id',
                                     'category_id',
                                     'category_id',
                                     'id');
    }
}

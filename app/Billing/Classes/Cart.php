<?php

namespace App\Billing\Classes;

class Cart extends Billing
{

    /**
     * 3rd Party Checkout.
     *
     * @return array
     */
    public function checkoutThirdParty($user, $products, $vendor, $invoiceNo, $purchaseDate = null)
    {
        $id = $user;

        if (is_object($user)) {
            $id = $user->id;
        }

        // validate products
        $products = array_map(function ($product) {

            // if id only, add default values
            if (is_numeric($product)) {
                return [
                    'id' => $product,
                    'quantity' => 1,
                ];
            }
        }, $products);

        $postData = [
            'client_customer_id' => $id,
            'products' => $products,
            'vendor' => $vendor,
            'invoice_number' => $invoiceNo,
        ];

        if ($purchaseDate) {
            $postData['date_purchased'] = $purchaseDate;
        }

        $response = $this->postData('cart-3rd-party', $postData);

        $code = $response->getStatusCode();
        $json = json_decode($response->getBody());

        if ($code == self::HTTP_OK) {

            return $this->standardResponse('Customer\'s purchases were successfully posted', $json, true);

        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {

            return $this->standardResponse('Please correct the following customer request errors:', $json, false);
        }

        return $this->standardResponse("An error occurred fetching the customer\'s purchases. Please try again later. $code".print_r($json, true),
            null, false);

    }
}

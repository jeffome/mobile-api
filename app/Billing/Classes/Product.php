<?php

namespace App\Billing\Classes;

class Product extends Billing
{
    public function get($attributes = [], $track_id = null, $responseOnly = false)
    {
        if($track_id) {
            $attributes['client_track_id'] = $track_id;
        }
        $response = $this->getData('products', $attributes);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());

        if ($responseOnly === true) {
            if ($code == self::HTTP_OK && is_array($json)) {
                return $json;
            }
            return [];
        }

        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Products were successfully fetched', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            return $this->standardResponse('Please correct the following errors:', $json, false);
        } elseif ($code == 433) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            if (is_string($json)) {
                return $this->standardResponse('An error occurred fetching the products. Please try again later.' . $json, $json, false);
            }
        }

        return $this->standardResponse('An error occurred fetching the products. Please try again later.', null, false);
    }

    public function find($id)
    {
        $url      = 'product/' . $id;
        $response = $this->getData($url);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());

        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product was successfully fetched', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            return $this->standardResponse('Please correct the following errors:', $json, false);
        } elseif ($code == 433) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            return $this->standardResponse('An error occurred fetching the product. Please try again later.', $json, false);
        }

        return $this->standardResponse('An error occurred fetching the product. Please try again later.', null, false);
    }

    public function create($details)
    {
        $response = $this->postData('product', $details);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product was successfully added', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        }

        return $this->standardResponse('An error occurred adding the product. Please try again later.', null, false);
    }

    public function update($id, $details)
    {
        $url      = 'product/' . $id;
        $response = $this->putData($url, $details);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product was successfully changed', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            return $this->standardResponse('An error occurred changing the product. Please try again later.', $json, false);
        }

        return $this->standardResponse('An error occurred changing the product. Please try again later.', null, false);
    }

    public function updateStock($id, $details)
    {
        $url      = 'product/' . $id . '/stock';
        $response = $this->putData($url, $details);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product\'s stock was successfully updated', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            return $this->standardResponse('An error occurred updating the product\'s stock. Please try again later.', $json, false);
        }

        return $this->standardResponse('An error occurred updating the product\'s stock. Please try again later.', null, false);
    }

    public function updateRestrictions($id, $details)
    {
        $url      = 'product/' . $id . '/restrictions';
        $response = $this->putData($url, $details);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product\'s restrictions were successfully updated', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            return $this->standardResponse('An error occurred updating the product\'s restrictions. Please try again later.', $json, false);
        }

        return $this->standardResponse('An error occurred updating the product\'s restrictions. Please try again later.', null, false);
    }

    public function activate($id)
    {
        return $this->changeActivationStatus($id, 1);
    }

    private function changeActivationStatus($id, $status, $attributeValue = '')
    {
        $url     = 'product/' . $id . '/active';
        $details = [
            'active' => $status,
        ];
        if (isset($attributeValue) && !empty($attributeValue)) {
            $details['attribute_value'] = $attributeValue;
        }
        $response = $this->putData($url, $details);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Product\'s activation status was successfully updated', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        } elseif ($code == self::HTTP_NOT_FOUND) {
            return $this->standardResponse('An error occurred updating the product\'s activation status. Please try again later.', $json, false);
        }

        return $this->standardResponse('An error occurred updating the product\'s activation status. Please try again later.', null, false);
    }

    public function deactivate($id)
    {
        return $this->changeActivationStatus($id, 0);
    }

    public function activateAttributeByValue($id, $attributeValue)
    {
        return $this->changeActivationStatus($id, 1, $attributeValue);
    }

    public function deactivateAttributeByValue($id, $attributeValue)
    {
        return $this->changeActivationStatus($id, 0, $attributeValue);
    }

    public function first($attributes = [], $track_id = null)
    {
        $output = self::get($attributes, $track_id);
        if ($output && $output['ok'] && isset($output['response'][0]) && is_object($output['response'][0])) {
            return $output['response'][0];
        }
        return null;
    }

    public function getAndKeyBy($attributes = [], $key, $trackId = null)
    {
        $output = self::get($attributes, $trackId);
        if ($output && $output['ok'] && count($output['response']) > 0) {
            return collect($output['response'])->keyBy($key);
        }
        return [];
    }

    public function getAndPluckBy($attributes = [], $key = 'id')
    {
        return (array) collect($this->get($attributes, \Helper::getCurrentTrackContext(), true))->pluck($key)->all();
    }

    public function findByAffiliate($affiliate)
    {

        if (!isset($affiliate->coupon->product_id) || $affiliate->coupon->product_id == null) {
            return null;
        }

        $id = (int) $affiliate->coupon->product_id;

        $output = self::find($id);
        if ($output && $output['ok'] && isset($output['response']) && isset($output['response']->id)) {
            return $output['response'];
        }
        return null;
    }

    public function findByAffiliateCode($code)
    {
        $affiliate = null;
        if (empty($code) || $code == null) {
            return null;
        }

        $response = \Affiliate::find($code);
        if ($response && $response['ok'] && is_object($response['response'])) {
            $affiliate = $response['response'];
        }

        return $this->findByAffiliate($affiliate);
    }

    public function getRevenueByDate($from, $to)
    {
        $url      = 'product/revenue/' . $from . '/' . $to;
        $response = $this->getData($url);
        $code     = $response->getStatusCode();
        $json     = json_decode($response->getBody());
        if ($code == self::HTTP_OK) {
            return $this->standardResponse('Revenue was successfully fetched', $json, true);
        } elseif ($code == self::HTTP_UNPROCESSABLE_ENTITY) {
            return $this->standardResponse('Please correct the following errors:', $json, false);
        } elseif ($code == 433) {
            if (is_string($json)) {
                return $this->standardResponse($json, $json, false);
            } elseif (property_exists($json, 'error')) {
                return $this->standardResponse($json->error, $json, false);
            }
        }

        return $this->standardResponse('An error occurred fetching the revenue. Please try again later.', null, false);
    }
}

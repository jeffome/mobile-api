<?php

namespace App\Billing\Classes;

use Authentication;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use GuzzleHttp\Client;
use Lukasoppermann\Httpstatus\Httpstatuscodes;

class Billing implements Httpstatuscodes
{
    protected $client;

    public function __construct()
    {
        // Establish a guzzle client for use in all API calls
        $this->client = new Client([
            'base_uri' => Config::get('billing.url.base'),
        ]);
    }

    /**
     * wrapper function for posting data.
     *
     * @param string $url
     * @param array $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function postData($url, $data = [])
    {
        $headers = [
            'Accept'        => 'application/json',
            'Authorization' => "Bearer {$this->getToken()}",
        ];
        $url = Config::get('billing.url.prefix') . $url;

        return $this->client->post($url, [
            'headers'     => $headers,
            'form_params' => $data,
            'http_errors' => false,
            'exceptions'  => false,
        ]);
    }

    /**
     * Recursive function that makes several attempts to get a valid API access token.
     *
     * @param int $callLevel
     *
     * @return bool|string
     *
     * @internal param int $stackLevel
     */
    protected function getToken($callLevel = 0)
    {
        $key = DB::table('api_keys')
                ->where('property_id', config('billing.property.id'))
                ->orderBy('id', 'ASC')->first();
        if ($key && !$this->hasTokenExpired($key)) {
            return $key->access_token;
        } else {
            $keyRefreshed = Authentication::refreshKey();
            if ($keyRefreshed && $callLevel <= 2) {
                return $this->getToken(++$callLevel);
            }
        }

        return false;
    }

    /**
     * Checks if a key has expired.
     *
     * @param DB record $key
     *
     * @return bool
     */
    protected function hasTokenExpired($key)
    {
        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', $key->created_at)->addSeconds($key->expires_in)->isPast();
        } catch (Exception $e) {
            \Bugsnag::notifyException($e);

            return false;
        }

        return true;
    }

    /**
     * wrapper function for put data.
     *
     * @param string $url
     * @param array $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function putData($url, $data = [])
    {
        $headers = [
            'Accept'        => 'application/json',
            'Authorization' => "Bearer {$this->getToken()}",
            'Content-Type'  => 'application/x-www-form-urlencoded',
        ];
        $url = Config::get('billing.url.prefix') . $url;

        return $this->client->put($url, [
            'headers'     => $headers,
            'form_params' => $data,
            'http_errors' => false,
            'exceptions'  => false,
        ]);
    }

    /**
     * wrapper function for getting data.
     *
     * @param string $url
     * @param array $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function getData($url, $params = [])
    {
        $headers = [
            'Accept'        => 'application/json',
            'Authorization' => "Bearer {$this->getToken()}",
        ];

        $url = Config::get('billing.url.prefix') . $url;
        $url .= '?' . http_build_query($params);

        return $this->client->get($url, [
            'headers'     => $headers,
            'http_errors' => false,
            'exceptions'  => false,
        ]);
    }

    /**
     * wrapper function for deleting data.
     *
     * @param string $url
     * @param array $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     *
     * @internal param array $params
     */
    protected function deleteData($url, $data = [])
    {
        $headers = [
            'Accept'        => 'application/json',
            'Authorization' => "Bearer {$this->getToken()}",
            'Content-Type'  => 'application/x-www-form-urlencoded',
        ];
        $url = Config::get('billing.url.prefix') . $url;

        return $this->client->delete($url, [
            'headers'     => $headers,
            'form_params' => $data,
            'http_errors' => false,
            'exceptions'  => false,
        ]);
    }

    /**
     * Handle guzzle error response.
     *
     * @param $e
     */
    protected function handleGuzzleException($e)
    {
        echo "\n\n" . $e->getMessage() . "\n\n";
    }


    /**
     * Standard Billing response.
     *
     * @param string $message
     * @param object $responseObject
     * @param bool $success
     *
     * @return array
     */
    protected function standardResponse($message, $responseObject, $success = true)
    {
        return [
            'ok'       => $success === true,
            'message'  => $message,
            'response' => $responseObject,
        ];
    }

}

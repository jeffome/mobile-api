<?php

namespace App;

use Cache;
use Illuminate\Support\Carbon;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	protected $fillable = ['name', 'display_name'];

	public function trackProductPermissions()
	{
		return $this->hasMany(\App\TrackProductPermissions::class, 'permission_id', 'id');
	}

	public static function forLifetimeFlashbackAccess()
	{
		return Cache::remember(
			__METHOD__,
			Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY,
			function () {
				return static::where('name', 'can-access-lifetime-flashback')->first();
			}
		);
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Question Model
 *
 * @SWG\Definition(title="Question", type="object", description="Question object returned by API")
 */
class QBankQuestion extends Model
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="course_id", type="integer"),
     * @SWG\Property(property="category_id", type="integer"),
     * @SWG\Property(property="topic_id", type="integer"),
     * @SWG\Property(property="physician_tasks", type="string"),
     * @SWG\Property(property="vignette", type="string"),
     * @SWG\Property(property="interrogatory", type="string"),
     * @SWG\Property(property="Answer1", type="string"),
     * @SWG\Property(property="Answer2", type="string"),
     * @SWG\Property(property="Answer3", type="string"),
     * @SWG\Property(property="Answer4", type="string"),
     * @SWG\Property(property="Answer5", type="string"),
     * @SWG\Property(property="Answer6", type="string"),
     * @SWG\Property(property="Answer7", type="string"),
     * @SWG\Property(property="Answer8", type="string"),
     * @SWG\Property(property="correctanswer", type="integer"),
     * @SWG\Property(property="explanation", type="string"),
     * @SWG\Property(property="teaching_point", type="string"),
     * @SWG\Property(property="comments", type="string"),
     * @SWG\Property(property="stats", type="object", description="Global result statistics for this question",
     *      @SWG\Property(property="answercorrect", type="integer"),
     *      @SWG\Property(property="answertotal", type="integer"),
     *      @SWG\Property(property="Answer1", type="integer"),
     *      @SWG\Property(property="Answer2", type="integer"),
     *      @SWG\Property(property="Answer3", type="integer"),
     *      @SWG\Property(property="Answer4", type="integer"),
     *      @SWG\Property(property="Answer5", type="integer"),
     *      @SWG\Property(property="Answer6", type="integer"),
     *      @SWG\Property(property="Answer7", type="integer"),
     *      @SWG\Property(property="Answer8", type="integer"),
     * ),
     * @SWG\Property(property="results", type="object", description="Specific result for this user and question",
     *      @SWG\Property(property="answer", type="integer"),
     *      @SWG\Property(property="is_correct", type="boolean"),
     * )
     */

    protected $table = 'qbanks';

    protected $fillable = [
        "course_id",
        "category_id",
        "topic_id",
        "physician_tasks",
        "vignette",
        "interrogatory",
        "Answer1",
        "Answer2",
        "Answer3",
        "Answer4",
        "Answer5",
        "Answer6",
        "Answer7",
        "Answer8",
        "correctanswer",
        "explanation",
        "teaching_point",
        "source",
        "comments",
        "status",
        "reviewed_by",
        "approved_by",
        "created_by",
    ];

    protected $appends = ['is_bookmarked', 'stats', 'result'];

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function topic() {
        return $this->belongsTo('App\Topic');
    }

    public function results() {
        return $this->hasMany('App\QBankResult', 'qbank_id');
    }

    /**
     * Get the is_bookmarked attribute for this question
     *
     * @return boolean is_bookmarked attribute
     */
    public function getIsBookmarkedAttribute()
    {
        $user    = app('Dingo\Api\Auth\Auth')->user();
        $user    = User::findOrFail($user->id);

        $this->attributes['is_bookmarked'] = $user->markedqbanks->contains($this->id);
       
        return +($this->attributes['is_bookmarked']);
    }

    /**
     * Get module specific results for this question
     *
     * @param $module_id
     * @return \App\QBankResult|boolean
     */
    public function getModuleSpecificResult($module_id) {
        $module = Module::findOrFail($module_id);

        // Build new result model if answer attempt exists
        if(isset($module->answer_attempts) && isset(json_decode($module->answer_attempts, true)[$this->id])) {
            $attempt = json_decode($module->answer_attempts, true)[$this->id];

            $result = new QBankResult;
            $result->fill([
                'qbank_id'   => $this->id,
                'result'     => $attempt['is_correct'] ? QBankResult::CORRECT : QBankResult::WRONG,
                'is_correct' => $attempt['is_correct'],
                'answer'     => $attempt['answer']
            ]);

            return $result;
        }
        // Return false if no answer attempt
        return false;
    }

    /**
     * Return Question Result attribute
     *
     * @return \App\QBankResult|null
     */
    public function getResultAttribute() {
        if (!isset($this->attributes['result'])) {
            $this->setResultAttribute();
        }
        return $this->attributes['result'];
    }

    /**
     * Set result attribute with global or module results
     *
     * @param integer $module_id optional for module specific results
     * @return QBankResult or null
     */
    public function setResultAttribute($module_id = null)
    {
        // Set result attribute to module answer attempts if specified
        if ($module_id) {
            $result = $this->getModuleSpecificResult($module_id);
            $this->attributes['result'] = $result;
        }
        // Set result attribute to user specific global results
        else {
            $user    = app('Dingo\Api\Auth\Auth')->user();
            $user    = User::findOrFail($user->id);

            $result = $this->results()->where('user_id', $user->id)->first();
            $this->attributes['result'] = $result;
        }
        return $this->attributes['result'];
    }

    /**
     * Build global Question statistics
     *
     * @return object global question statistics
     */
    public function getStatsAttribute()
    {
        $all_results = QBankResult::where('qbank_id', $this->id)->get();

        $statistics = [
            'answercorrect' => $all_results->where('is_correct', true)->count(),
            'answertotal'   => $all_results->count(),
        ];

        // Build answer stats
        for ($answer_choice = 1; $answer_choice <= 8; $answer_choice++) {
            $statistics['Answer' . $answer_choice] = $all_results->where('answer', $answer_choice)->count();
        }

        $this->attributes['stats'] = $statistics;
        return $this->attributes['stats'];
    }

}

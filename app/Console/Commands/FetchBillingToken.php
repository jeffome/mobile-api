<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Authentication;
use Mail;
use Config;
use App\Mail\CronJobDeveloperEmail;

class FetchBillingToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ome:fetch-billing-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch a new oauth token from the billing API';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(PHP_EOL.'Fetching new billing API token'.PHP_EOL);

        if (Authentication::refreshKey()) {
            // $this->emailTheDeveloper(true);

            $this->info('Success');
        } else {
            $this->emailTheDeveloper(false);

            $this->error('No key generated');
        }
    }

    /**
     * Email the results to the developer
     *
     * @param bool $success
     */
    private function emailTheDeveloper($success = true)
    {

        $environment = ucfirst(\App::environment());
        $details = 'New Billing API token fetched successfully on '.$environment."\n\n";
        $subject = 'Fetch Billing API Token';

        if (! $success) {
            $details = $environment.' server was unable to fetch a new billing API token!'."\n\n";
            $subject = 'ACTION REQUIRED: Billing API token error';
        }

        $data = ['details' => $details, 
                 'duration' => 0,
                 'type' => $subject,
                 'name' => $subject,
                 'environment' => $environment
                ];

        $this->error(Config::get('mail.from.address'));

        Mail::send('emails.developer.cron', $data, function ($m) use ($data) {
            $m->from(Config::get('mail.from.address'), 'Your Application');
            $m->to(Config::get('mail.developer.address'), Config::get('mail.developer.name'))->subject('Cron Job Complete: ' . $data['type']);
        });
        
    }
}

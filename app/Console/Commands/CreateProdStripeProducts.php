<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe;
use Config;
use App\Topic;

class CreateProdStripeProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:sync-products-prod';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates products in stripe production from products in stripe staging while maintaining the same product ids';

    private $stagingSecret = '';

    private $prodSecret = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->stagingSecret = Config::get('stripe.stagingSecret');
        $this->prodSecret = Config::get('stripe.prodSecret');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stagingProducts = $this->getStagingProducts();
        $this->info(count($stagingProducts)." products found on staging");
        $prodProducts = $this->getProdProducts();
        $this->info(count($prodProducts)." products found on prod");
        
        $stagingById = $this->productsById($stagingProducts);
        $prodById = $this->productsById($prodProducts);

        $createProducts = array_filter($stagingById, function($obj) use ($prodById) {
            if (in_array($obj->id, array_keys($prodById))) {
                return false;
            }
            return true;
        });

        $this->info(count($createProducts)." products to be created");

        $this->createProducts($createProducts);
    }

    private function createProducts($products) {
        $this->setProd();
        foreach($products as $product) {
            $skus = $product->skus->data;
            if (count($skus) > 0) {
                $this->info("Create Product");
                $newProduct = [
                    "id" => $product->id,
                    "name" => $product->name,
                    "type" => $product->type,
                    "description" => empty($product->description) ? "No description" : $product->description,
                    "attributes" => $product->attributes,
                    "shippable" => $product->shippable,
                    "active" => $product->active,
                ];
                print_r($newProduct);
                $createdProduct = Stripe\Product::create($newProduct);              

                foreach($skus as $sku) {
                    $this->info("Create Sku");  
                    $newSku = [
                        "product" => $product->id,
                        "price" => $sku->price,
                        "currency" => $sku->currency,
                        "inventory" => [
                            "type" => "infinite",
                        ],
                        "id" => $sku->id,
                        "active" => $sku->active,
                    ];
                    print_r($newSku);
                    $createdSku = Stripe\Sku::create($newSku);
                }

            }
            
        }
    }

    private function productsById($products) {
        $keys = array_map(function($obj){return $obj->id;}, $products);
        return array_combine($keys, array_values($products));
    }

    private function setStaging() {
        Stripe\Stripe::setApiKey($this->stagingSecret);
    }

    private function setProd() {
        Stripe\Stripe::setApiKey($this->prodSecret);
    }

    private function getStagingProducts() {
        $this->info("getStagingProducts");
        $this->setStaging();
        return $this->getProducts();
    }

    private function getProdProducts() {
        $this->info("getProdProducts");
        $this->setProd();
        return $this->getProducts();
    }

    private function getProducts() {
        $productList = [];
        $fetchProducts = true;
        $options = ["limit" => 100];
        while($fetchProducts) {
            $products = Stripe\Product::all($options);
            $productList = array_merge($productList, $products->data);
            if ($products->has_more) {
                $lastProduct = end($products->data);
                $options["starting_after"] = $lastProduct->id;
            } else {
                $fetchProducts = false;
            }
        }
        return $productList;
    }

}

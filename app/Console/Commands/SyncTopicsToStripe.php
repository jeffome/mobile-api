<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stripe;
use Config;
use App\Topic;

class SyncTopicsToStripe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stripe:sync-topics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates products in stripe for all missing topics';

    private $prodSecret;
    private $stagingSecret;
    private $existingTopicIds;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->stagingSecret = Config::get('stripe.stagingSecret');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Pulling existing product information from stripe");
        $this->setStaging();
        $this->getMissingProducts();
        $this->createProducts();
    }

    private function createProducts() {
        $this->info("Comparing existing products against topics");
        $topics = Topic::where('is_active_website', true)->whereNotIn('id', $this->existingTopicIds)->get();
        $missingTopicCount = count($topics);
        if (count($topics) > 0) {
            $this->info("Creating $missingTopicCount missing products...");
        } else {
            $this->info("No missing products were found.");
        }
        foreach($topics as $topic) {
            $product = Stripe\Product::create([
                "name" => "Lesson: " . $topic->name,
                "type" => "good",
                "description" => "Mobile App Purchase: ". $topic->name,
                "attributes"=> [],
                "shippable" => "true",
                "active" => "true"
            ]);
            $this->info("Created product Lesson: " . $topic->name);
            $sku = Stripe\Sku::create([
                "product" => $product->id,
                "price" => 299,
                "currency" => "usd",
                "inventory" => [
                    "type" => "infinite",
                ],
                "id" => "mobile_lesson_" . $topic->id,
                "active" => "true",
            ]);
            $this->info("Created sku mobile_lesson_" . $topic->id);
        }
    }

    private function getMissingProducts() {
        $products = $this->getProducts();
        $this->info(count($products)." products found");
        $productsById = $this->productsById($products);
        $this->existingTopicIds = $productsById;
    }

    private function setStaging() {
        Stripe\Stripe::setApiKey($this->stagingSecret);
    }

    private function getProducts() {
        $productList = [];
        $fetchProducts = true;
        $options = ["limit" => 100];
        while($fetchProducts) {
            $products = Stripe\Product::all($options);
            $productList = array_merge($productList, $products->data);
            if ($products->has_more) {
                $lastProduct = end($products->data);
                $options["starting_after"] = $lastProduct->id;
            } else {
                $fetchProducts = false;
            }
        }
        return $productList;
    }

    private function productsById($products) {
        $existing = array_map(function($product){
            $skus = $product->skus;
            if ($skus && count($skus->data) > 0) {
                $firstSku = $skus->data[0];
                if ($firstSku) {
                    if(substr($firstSku->id, 0, 13) === "mobile_lesson") {
                        return (int) substr($firstSku->id, 14);
                    }
                    return null;
                }
            }
            return null;
        }, $products);
        $filtered = array_filter($existing, function ($val) {
            return is_int($val);
        });
        return $filtered;
    }
    
}

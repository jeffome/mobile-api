<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorReport extends Model
{
    protected $fillable = ['object_id', 'type', 'reason', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QBankQuestion;

/**
 * Question Modules
 */
class Module extends Model
{
    const TIMED_TEST_SECONDS_PER_QUESTION = 90;

    protected $table = 'modules';

    protected $fillable = [
        "id",
        "user_id",
        "testmode",
        "questionmode",
        "questionids",
        "complete",
        "starttime",
        "timerlength",
        "numcorrect",
        "finishtime",
        "answer_attempts",
        "settings",
        "settings->display_name",
        "settings->active",
    ];

    protected $casts = [
        'settings' => 'array',
    ];

    protected $appends = ['questions', 'numcorrect'];

    public static function CreateFromCategoryId($category_id) {
        $module = new Module();
        // QuestionIds is a comma delim string ex: 1,2,3,4,5
        $module->questionids = Category::findOrFail($category_id)
                                        ->questions()
                                        ->where('status', 'Approved')
                                        ->get()
                                        ->pluck('id')
                                        ->implode(',');
        return $module;
    }

    /**
     * Get module user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Count Module Questions
     *
     * @return int
     */
    public function countQuestions() {
        return count($this->questions);
    }

    private function getTopicsInTrack() {
        $topicIdsInTrack = $this->user->careerTrack->getTopics()->pluck('id')->toArray();
        return $topicIdsInTrack;
    }

    public function getQuestionIds() {
        return collect(explode(',' ,$this->questionids));
    }

    public function getTrackFilteredQuestionIds() {
        $topicIds = $this->getTopicsInTrack();
        $inTrackQuestions = QBankQuestion::whereIn('id', $this->getQuestionIds()->toArray())
                                  ->whereIn('topic_id', $topicIds)
                                  ->pluck('id');
        return $inTrackQuestions;
    }

    /**
     * Get Questions in Module
     *
     * @return array of Question objects in Module
     */
    public function getQuestions() {
        return QBankQuestion::whereIn('id', $this->getQuestionIds()->toArray())->get();
    }

    /**
     * Get global Results for this module
     *
     * @param $user_id
     * @return array of question result objects
     */
    public function getGlobalResults($user_id) {
        return QBankResult::where('user_id', $user_id)
            ->whereIn('qbank_id', $this->getQuestionIds()->toArray())
            ->get();
    }

    /**
     * Retrieve display_name from settings json
     *
     * @return string display_name
     */
    protected function getDisplayNameAttribute() {
        if (isset($this->settings['display_name'])) {
            return $this->settings['display_name'];
        }
        return '';
    }

    /**
     * Retrieve active from settings json
     *
     * @return boolean active
     */
    protected function getActiveAttribute() {
        if (isset($this->settings['active'])) {
            return $this->settings['active'];
        }
        return 1;
    }

    /**
     * Get Module Questions Attribute
     *
     * @param boolean $moduleResults
     * @param boolean $retrieveQuestions
     * @return array of question objects
     */
    public function getQuestionsAttribute($retrieveQuestions = false, $moduleResults = false) {
        $this->attributes['questions'] = [];
        
        if ($retrieveQuestions) {
            $questions = $this->getQuestions();
            if ($moduleResults) {
                foreach ($questions as $question) {
                    $question->setResultAttribute($this->id);
                }                
            }
            $this->attributes['questions'] = $questions;
        }

        return $this->attributes['questions'];
    }

    /**
     * Calculate number of correct results for module
     *
     * @return int
     */
    public function getNumcorrectAttribute() {

        // Module specific
        if($this->id && $all_attempts = json_decode($this->answer_attempts, true)) {
            $correct_answers = array_filter($all_attempts, function ($var) {
                return $var['is_correct'];
            });
            $num_correct = count($correct_answers);
        }

        // Global results
        else {
            $user = app('Dingo\Api\Auth\Auth')->user();
            $user = User::findOrFail($user->id);

            $global_results = $this->getGlobalResults($user->id);
            $num_correct= $global_results->where('is_correct', true)->count();
        }

        $this->attributes['numcorrect'] = $num_correct;
        return $this->attributes['numcorrect'];
    }

    /**
     * Creates or updates Module answer_attempts JSON
     *
     * @param $qbank_id integer
     * @param $answer integer
     * @param $is_correct boolean
     */
    public function addAnswerAttempt($qbank_id, $answer, $is_correct) {
        $attempts = json_decode($this->answer_attempts, true);
        $attempts ? null : $attempts = [];

        $attempts[$qbank_id] = [
            'answer'          => $answer,
            'is_correct'      => $is_correct,
        ];

        $this->update(['answer_attempts' => json_encode($attempts)]);
    }

}

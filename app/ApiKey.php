<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
    protected $fillable = [
        'access_token',
        'expires_in',
        'property_id',
        'refresh_token',
        'token_type',
    ];

    public static function byProperty($property)
    {
        return Cache::remember(
            "{$property}-passport-token",
            Carbon::MINUTES_PER_HOUR * Carbon::HOURS_PER_DAY * Carbon::DAYS_PER_WEEK,
            function () use ($property) {
                \Artisan::call('ome:fetch-passport-token');
                return Cache::get("{$property}-passport-token");
            }
        );
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{

    protected $table = 'flashcard_user_answers';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_flipped_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flashcard_id', 'user_id', 'confidence_level', 'last_flipped_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function flashcard()
    {
        return $this->belongsTo('App\Flashcard');
    }
}

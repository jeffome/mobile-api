<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/loaderio-5f608553503f68fa1d33cedff0e75838', function() {
    return 'loaderio-5f608553503f68fa1d33cedff0e75838';
});

$api = app('Dingo\Api\Routing\Router');

$throttle = [
    'middleware' => 'api.throttle',
    'limit' => config('api.settings.limit'),
    'expires' => config('api.settings.expires'),
];

$api->version('v1', $throttle, function ($api) {
    $api->get('swagger/yaml', 'App\Http\Controllers\DocumentationController@swagger');
    // institutions
    $api->get('institutions','App\Http\Controllers\InstitutionController@listInstitutions');
    // professions
    $api->get('professions','App\Http\Controllers\ProfessionController@listProfessions');
    //countries
    $api->get('country','App\Http\Controllers\CountryController@listCountries');
    $api->get('country/institutions','App\Http\Controllers\CountryController@countriesWithInstitution');
    // tracks
    $api->get('tracks', 'App\Http\Controllers\TrackController@getTracks');
    // User: registration
    $api->post('signup', 'App\Http\Controllers\RegistrationController@signUp');
    $api->post('signup-social', 'App\Http\Controllers\RegistrationController@socialSignup');
    $api->post('password-reset', 'App\Http\Controllers\RegistrationController@passwordReset');
    $api->post('password-forgot', 'App\Http\Controllers\RegistrationController@sendPasswordResetEmail');
    $api->post('resend-activation', 'App\Http\Controllers\RegistrationController@resendActivationEmail');
    // User: authentication
    $api->get('authenticate', 'App\Http\Controllers\AuthenticateController@index');
    $api->post('authenticate', 'App\Http\Controllers\AuthenticateController@authenticateFromEmail');
    $api->post('authenticate/social', 'App\Http\Controllers\AuthenticateController@authenticateFromSocial');

    $api->group(['middleware' => ['jwt.auth', 'enforceCareerTrack']], function ($api) {
        // "Static" Resources
        $api->get('clerkships', 'App\Http\Controllers\ClerkshipController@index');
        $api->get('categoriesWithStats', 'App\Http\Controllers\CategoryController@withStats');
        $api->get('topics', 'App\Http\Controllers\TopicController@index');
        $api->get('topic/asset-url/{contentType}/{topicId}', 'App\Http\Controllers\TopicController@getAssetUrl');
        $api->post('confirm-download/{contentType}/{topicId}', 'App\Http\Controllers\TopicController@confirmDownload');
        /** @deprecated */
        $api->get('topic/asset/{contentType}/{topicId}/{download?}', 'App\Http\Controllers\TopicController@getAsset');
        $api->get('flashcards', 'App\Http\Controllers\FlashcardController@index');
        $api->resource('user-answers', 'App\Http\Controllers\UserAnswerController', ['only' => ['index', 'store']]);
        $api->resource('custom-decks', 'App\Http\Controllers\CustomDeckController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
        // User
        $api->post('users/link-social', 'App\Http\Controllers\UserController@linkSocial');
        $api->post('users/unlink-social', 'App\Http\Controllers\UserController@unlinkSocial');
        $api->post('users/set-trial-end', 'App\Http\Controllers\UserController@setTrialEndDate');
        $api->post('users/set-avatar', 'App\Http\Controllers\UserController@setAvatar');
        $api->post('users/set-career-track', 'App\Http\Controllers\UserController@setCareerTrack');
        // User: settings
        $api->post('users/settings', 'App\Http\Controllers\UserSettingController@setSettings');
        $api->get('users/settings', 'App\Http\Controllers\UserSettingController@getSettings');
        // User: metrics
        $api->get('metrics/recent-topics', 'App\Http\Controllers\MetricController@getRecentTopics');
        $api->post('metrics/recent-topics/{id}', 'App\Http\Controllers\MetricController@postRecentTopics');
        $api->post('metrics/recent-topics/watched/{id}', 'App\Http\Controllers\MetricController@resetOrCreateRecentTopic');
        $api->get('metrics/studied-topics', 'App\Http\Controllers\MetricController@studiedTopics');
        $api->get('metrics/daily-answers', 'App\Http\Controllers\DailyAnswerTallyController@getTodaysAnswers');
        $api->post('metrics/daily-answers/set-alert', 'App\Http\Controllers\DailyAnswerTallyController@setAlertShownAt');
        $api->post('metrics/daily-answers', 'App\Http\Controllers\DailyAnswerTallyController@incrementDailyAnswers');
        $api->post('metrics', 'App\Http\Controllers\MetricController@createOrUpdateMetric');
        // User: activity
        $api->get('user/activity/video', 'App\Http\Controllers\UserActivityController@getRecentlyWatchedVideos');
        $api->get('user/activity/qbank/summary', 'App\Http\Controllers\UserActivityController@getQbankSummary');
        // Qbank: Retrieve, Answer, and Bookmark Questions
        $api->put('qbank/question/{question_id}/answered', 'App\Http\Controllers\QuestionController@answerQuestion');
        $api->put('qbank/bookmark/question/{question_id}', 'App\Http\Controllers\QuestionController@bookmarkQuestion');
        // Qbank: Retrieve modules
        $api->get('qbank/module', 'App\Http\Controllers\ModuleController@getAllModules');
        $api->get('qbank/module/{moduleId}', 'App\Http\Controllers\ModuleController@getModulesById');
        $api->get('qbank/category/{categoryId}', 'App\Http\Controllers\ModuleController@makeTempModuleByCategory');
        // Qbank: Create, Update, and Delete Modules
        $api->post('qbank/module', 'App\Http\Controllers\ModuleController@createModule');
        $api->put('qbank/module/{module_id}', 'App\Http\Controllers\ModuleController@updateModule');
        $api->delete('qbank/module/{module_id}', 'App\Http\Controllers\ModuleController@deleteModule');
        // Qbank: Module in-use endpoints
        $api->put('qbank/module/{module_id}/start', 'App\Http\Controllers\ModuleController@moduleStarted');
        $api->put('qbank/module/{module_id}/finish', 'App\Http\Controllers\ModuleController@moduleFinished');
        $api->put('qbank/module/{module_id}/retake', 'App\Http\Controllers\ModuleController@retakeModule');
        // Flashback: system decks
        $api->get('system-decks', 'App\Http\Controllers\SystemDeckController@index');
        $api->get('system-decks/{systemDeckTypeId}', 'App\Http\Controllers\SystemDeckController@show');
        $api->put('system-decks/{systemDeckTypeId}', 'App\Http\Controllers\SystemDeckController@updateSystemDecks');
        // Flashback: deck states
        $api->get('deck-states', 'App\Http\Controllers\DeckStateController@getStates');
        $api->post('deck-states', 'App\Http\Controllers\DeckStateController@setState');
        // Intern Bootcamp
        $api->resource('intern-bootcamp/sections', 'App\Http\Controllers\InternBootcamp\SectionController', ['only' => ['index', 'show']]);
        $api->resource('intern-bootcamp/videos', 'App\Http\Controllers\InternBootcamp\VideoController', ['only' => ['index', 'show']]);
        $api->get('intern-bootcamp/assets/video_cc/{id}', 'App\Http\Controllers\InternBootcamp\VideoController@getVideoClosedCaption');
        // Error Reports && Feedback
        $api->post('error-reports', 'App\Http\Controllers\ErrorReportController@store');
        $api->post('feedback', 'App\Http\Controllers\FeedbackController@store');
        // Billing: microtransactions
        $api->post('microtransactions', 'App\Http\Controllers\MicroTransactionController@postMicroTransaction');
        // Billing: products
        $api->get('products', 'App\Http\Controllers\ProductsController@getProducts');
        $api->get('product-access', 'App\Http\Controllers\ProductsController@getProductAccess');
    });
});

<?php

return [

    'subdomain' => env('RECURLY_SUBDOMAIN', 'onlinemeded'),
    'token'     => env('RECURLY_TOKEN', 'xxx'),
];

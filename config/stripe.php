<?php

return [

    'stagingSecret' => env('STRIPE_SECRET_STAGE'),
    'prodSecret' => env('STRIPE_SECRET_PROD')

];

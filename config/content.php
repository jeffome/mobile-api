<?php

return [
    's3_bucket_url'         => env('FILE_S3_URL', 'https://s3-us-west-2.amazonaws.com/') . env('FILE_S3_ENVIRONMENT', 'onlinemeded-staging/'),
    'audio_path'            => env('CONTENT_AUDIO_PATH','onlinemeded/content/audio/'),
    'batch_path'            => env('CONTENT_BATCH_PATH','onlinemeded/content/batch/'),
    'images_path'           => env('CONTENT_IMAGES_PATH','onlinemeded/content/images/'),
    'notes_path'            => env('CONTENT_NOTES_PATH','onlinemeded/content/notes/'),
    'previews_path'         => env('CONTENT_PREVIEWS_PATH','onlinemeded/content/previews/'),
    'studyschedules_path'   => env('CONTENT_STUDYSCHEDULES_PATH','onlinemeded/content/studyschedules/'),
    'whiteboards_path'      => env('CONTENT_WHITEBOARDS_PATH','onlinemeded/content/whiteboards/'),
];
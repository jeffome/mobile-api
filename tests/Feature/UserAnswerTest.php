<?php

namespace Tests\Feature;

use App\UserAnswer;

class UserAnswerTest extends TestCase
{
    protected $testAnswer = [
        'flashcard_id'     => 1,
        'confidence_level' => 1,
        'last_flipped_at'  => '2016-12-16 06:27:12',
        'updated_at'       => '2016-12-17 10:12:30',
    ];

    private function createTestAnswer()
    {
        $this->deleteTestAnswer();

        return UserAnswer::create([
            'flashcard_id'     => $this->testAnswer['flashcard_id'],
            'confidence_level' => $this->testAnswer['confidence_level'],
            'last_flipped_at'  => $this->testAnswer['last_flipped_at'],
            'updated_at'       => $this->testAnswer['updated_at'],
            'user_id'          => config('test.id'),
        ]);
    }

    private function deleteTestAnswer()
    {
        return UserAnswer::where('user_id', config('test.id'))->delete();
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('user-answers');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testIndex()
    {
        $this->createTestAnswer();

        $token    = $this->getToken();
        $response = $this->client->get('user-answers?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(is_array($json->data));
        $this->assertTrue(count($json->data) > 0);

        foreach ($json->data as $answer) {
            $this->assertEquals($answer->user_id, config('test.id'));
        }

        $this->deleteTestAnswer();
    }

    public function testCreateSuccess()
    {
        $answer = $this->deleteTestAnswer();

        $token    = $this->getToken();
        $response = $this->client->post('user-answers?token=' . urlencode($token), [
            'form_params' => $this->testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $answer = UserAnswer::where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($answer->confidence_level, $this->testAnswer['confidence_level']);
        $this->assertEquals($answer->flashcard_id, $this->testAnswer['flashcard_id']);

        $this->deleteTestAnswer();
    }

    public function testCreateNoDuplicates()
    {
        $answer = $this->deleteTestAnswer();

        $token    = $this->getToken();
        $response = $this->client->post('user-answers?token=' . urlencode($token), [
            'form_params' => $this->testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $response = $this->client->post('user-answers?token=' . urlencode($token), [
            'form_params' => $this->testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $count = UserAnswer::where('user_id', config('test.id'))->where('flashcard_id', $this->testAnswer['flashcard_id'])->count();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($count, 1);

        $this->deleteTestAnswer();
    }

    public function testCreateMissingFlashcard()
    {
        $answer = $this->createTestAnswer();

        $testAnswer = $this->testAnswer;
        unset($testAnswer['flashcard_id']);

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->flashcard_id));
        $this->assertTrue(count($json->errors->flashcard_id) > 0);

        $this->deleteTestAnswer();
    }

    public function testCreateInvalidFlashcard()
    {
        $answer = $this->createTestAnswer();

        $testAnswer                 = $this->testAnswer;
        $testAnswer['flashcard_id'] = 2345456;

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->flashcard_id));
        $this->assertTrue(count($json->errors->flashcard_id) > 0);

        $this->deleteTestAnswer();
    }

    public function testDefaultConfidenceLevel()
    {
        $answer = $this->createTestAnswer();

        $testAnswer = $this->testAnswer;
        unset($testAnswer['confidence_level']);

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $answer = UserAnswer::where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($answer->convidence_level, 0);

        $this->deleteTestAnswer();
    }

    public function testUpdateSuccess()
    {
        $answer = $this->createTestAnswer();

        $testAnswer                     = $this->testAnswer;
        $testAnswer['confidence_level'] = 5;
        $testAnswer['last_flipped_at']  = '2017-01-17 10:12:30';
        $testAnswer['updated_at']       = '2017-02-16 03:09:23';

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $answer = UserAnswer::where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($answer->confidence_level, $testAnswer['confidence_level']);
        $this->assertEquals($answer->flashcard_id, $testAnswer['flashcard_id']);

        $this->assertEquals($json->data->last_flipped_at, $testAnswer['last_flipped_at']); // compare against response for timezone conversion
        $this->assertEquals($json->data->updated_at, $testAnswer['updated_at']); // compare against response for timezone conversion
        $this->assertNotEquals($answer->last_flipped_at, $testAnswer['last_flipped_at']);
        $this->assertNotEquals($answer->updated_at, $testAnswer['updated_at']);

        $this->deleteTestAnswer();
    }

    public function testUpdateMissingFlashcard()
    {
        $answer = $this->createTestAnswer();

        $testAnswer = $this->testAnswer;
        unset($testAnswer['flashcard_id']);

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->flashcard_id));
        $this->assertTrue(count($json->errors->flashcard_id) > 0);

        $this->deleteTestAnswer();
    }

    public function testUpdateInvalidFlashcard()
    {
        $answer = $this->createTestAnswer();

        $testAnswer                 = $this->testAnswer;
        $testAnswer['flashcard_id'] = 1234455666;

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->flashcard_id));
        $this->assertTrue(count($json->errors->flashcard_id) > 0);

        $this->deleteTestAnswer();
    }

    public function testUpdateMissingConfidenceLevel()
    {
        $answer = $this->createTestAnswer();

        $testAnswer = $this->testAnswer;
        unset($testAnswer['confidence_level']);

        $token    = $this->getToken();
        $response = $this->client->post('user-answers/?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $answer = UserAnswer::where('flashcard_id', $testAnswer['flashcard_id'])->where('user_id', config('test.id'))->first();

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($answer->confidence_level, $this->testAnswer['confidence_level']);
        $this->assertEquals($answer->flashcard_id, $testAnswer['flashcard_id']);

        $this->deleteTestAnswer();
    }

    public function testCorrectTimestampBeforeDaylightSavingsTime()
    {
        $this->deleteTestAnswer();

        $testAnswer = $this->testAnswer;
        $testAnswer['last_flipped_at'] = '2017-01-21 08:06:21';
        $testAnswer['updated_at']      = '2017-01-20 08:06:21';

        $token = $this->getToken();
        $response = $this->client->post('user-answers?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->last_flipped_at, $testAnswer['last_flipped_at']);
        $this->assertEquals(strtotime($json->data->last_flipped_at), strtotime($testAnswer['last_flipped_at']));
        $this->assertEquals($json->data->updated_at, $testAnswer['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testAnswer['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('user-answers?token=' . urlencode($token));
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );


        $this->assertTrue(property_exists($json, 'data'));
        $firstAnswer = $json->data[0];
        $this->assertEquals($firstAnswer->last_flipped_at, $testAnswer['last_flipped_at']);
        $this->assertEquals(strtotime($firstAnswer->last_flipped_at), strtotime($testAnswer['last_flipped_at']));
        $this->assertEquals($firstAnswer->updated_at, $testAnswer['updated_at']);
        $this->assertEquals(strtotime($firstAnswer->updated_at), strtotime($testAnswer['updated_at']));

        $this->deleteTestAnswer();
    }

    public function testCorrectTimestampAfterDaylightSavingsTime()
    {
        $this->deleteTestAnswer();

        $testAnswer = $this->testAnswer;
        $testAnswer['last_flipped_at'] = '2017-03-25 18:06:21';
        $testAnswer['updated_at']      = '2017-03-25 06:25:55';

        $token = $this->getToken();
        $response = $this->client->post('user-answers?token=' . urlencode($token), [
            'form_params' => $testAnswer,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->last_flipped_at, $testAnswer['last_flipped_at']);
        $this->assertEquals(strtotime($json->data->last_flipped_at), strtotime($testAnswer['last_flipped_at']));
        $this->assertEquals($json->data->updated_at, $testAnswer['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($testAnswer['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('user-answers?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $firstAnswer = $json->data[0];
        $this->assertEquals($firstAnswer->last_flipped_at, $testAnswer['last_flipped_at']);
        $this->assertEquals(strtotime($firstAnswer->last_flipped_at), strtotime($testAnswer['last_flipped_at']));
        $this->assertEquals($firstAnswer->updated_at, $testAnswer['updated_at']);
        $this->assertEquals(strtotime($firstAnswer->updated_at), strtotime($testAnswer['updated_at']));

        $this->deleteTestAnswer();
    }
}

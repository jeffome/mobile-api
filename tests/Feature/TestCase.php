<?php

namespace Tests\Feature;

use App\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Lukasoppermann\Httpstatus\Httpstatuscodes;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class TestCase extends BaseTestCase implements Httpstatuscodes
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $client;
    protected $baseUrl;
    protected $apiPrefix = '/api/v1/';

    public function assertArrayKeys($expectedKeys, $arr) {
      //assert that the returned row has all expected columns
      foreach ($expectedKeys as $key) {
        $this->assertArrayHasKey($key, $arr);
      }

      //assert that the returned row doesn't have any unexpected columns
      foreach (array_keys($arr) as $key) {
        $this->assertContains($key, $expectedKeys);
      }
    }

    public function setUp()
    {
        parent::setUp();

        $this->disableExceptionHandling();

        // required for the built-in laravel headless browser
        $this->baseUrl = config('app.url');

        $this->client = new \GuzzleHttp\Client([
            'base_uri'   => config('app.url') . $this->apiPrefix,
            'exceptions' => false,
        ]);
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../../bootstrap/app.php';

        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}
            public function report(\Exception $e) {}
            public function render($request, \Exception $e) {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(
            ExceptionHandler::class,
            $this->oldExceptionHandler
        );
        return $this;
    }

    public function getToken()
    {
        $response = $this->postJson($this->apiPrefix . 'authenticate', [
            'email'    => config('test.username'),
            'password' => config('test.password'),
        ]);

        $json = json_decode($response->content());
        // dd(__METHOD__ . ':' . __LINE__, $json);

        if ($json && isset($json->token)) {
            return $json->token;
        }

        return false;
    }

}

<?php

namespace Tests\Feature;

use App\QBankQuestion;
use App\QBankResult;

class QuestionsTest extends TestCase
{

    /**
     * PUT qbank/question/1/answered
     */
    public function testQuestionAnsweredNotAuthenticated()
    {
        $response = $this->client->put('qbank/question/1/answered', [
            'form_params' => ['answer' => 1],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    /**
     * PUT qbank/question/1/answered
     */
    public function testQuestionAnswered()
    {
        $token = $this->getToken();

        $response = $this->client->put('qbank/question/1/answered', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['answer' => 1],
        ]);
        $json = json_decode($response->getBody());

        QBankResult::findOrFail($json->data->id)->delete();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
    }

    /**
     * PUT qbank/bookmark/question/1
     */
    public function testSetBookmark()
    {
        $token = $this->getToken();

        $response = $this->client->put('qbank/bookmark/question/1', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['bookmark' => true],
        ]);
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'success'));
    }

    /**
     * PUT qbank/bookmark/question/1
     */
    public function testRemoveBookmark()
    {
        $token = $this->getToken();

        $response = $this->client->put('qbank/bookmark/question/1', [
            'headers' => ['Authorization' => 'Bearer ' . $token],
            'form_params' => ['bookmark' => false],
        ]);
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'success'));
    }

}

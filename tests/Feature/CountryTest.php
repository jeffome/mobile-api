<?php

namespace Tests\Feature;

class CountryTest extends TestCase
{
    public function testDefaults()
    {
        $response = $this->client->get('country');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        $firstItem = $testData[0];
        $this->assertArrayKeys(['code','name'], $firstItem);
    }

    public function testCountryWithInstitutions()
    {
        $response = $this->client->get('country/institutions');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        $firstItem = $testData[0];
        $this->assertArrayKeys(['code','name'], $firstItem);
    }

    public function testCountryWithInstitutionsSelectName()
    {
        $response = $this->client->get('country/institutions?select=name');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        $firstItem = $testData[0];
        $this->assertArrayKeys(['name'], $firstItem);
    }

    public function testCountryWithInstitutionsSearchUganda()
    {
        $response = $this->client->get('country/institutions?code=UG');
        $responseAll = $this->client->get('country/institutions');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $resAll = json_decode($responseAll->getBody(), true);
        $testDataAll = $resAll['data'];

        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
        //Verify that ugandan result contains less results than whole world
        $this->assertTrue(count($testDataAll) > count($testData));
        $firstItem = $testData[0];
        $this->assertArrayKeys(['name','code'], $firstItem);
    }

    public function test404()
    {
        $response = $this->client->get('country/IDONOTEXIST');

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }
}

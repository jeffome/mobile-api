<?php

namespace Tests\Feature;

use App\UserSetting;

class UserSettingTest extends TestCase
{
    protected $testSettings = [
        'include_step_2' => true,
        'include_step_3' => false,
        'timer_7_day'    => true,
        'timer_14_day'   => false,
        'timer_30_day'   => true,
        'updated_at'     => '2016-12-16 06:27:12',
    ];

    private function createUserSettings()
    {
        $this->clearUserSettings();

        return UserSetting::create([
            'user_id'        => config('test.id'),
            'include_step_2' => $this->testSettings['include_step_2'],
            'include_step_3' => $this->testSettings['include_step_3'],
            'timer_7_day'    => $this->testSettings['timer_7_day'],
            'timer_14_day'   => $this->testSettings['timer_14_day'],
            'timer_30_day'   => $this->testSettings['timer_30_day'],
        ]);
    }

    private function clearUserSettings()
    {
        return UserSetting::where('user_id', config('test.id'))->delete();
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('users/settings');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testSetUserSetting()
    {
        // start fresh
        $this->clearUserSettings();

        $token    = $this->getToken();
        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => $this->testSettings,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $settings = UserSetting::where('user_id', config('test.id'))->first();

        $this->assertTrue(is_object($settings));
        $this->assertEquals($settings->user_id, config('test.id'));
        $this->assertEquals($settings->include_step_2, $this->testSettings['include_step_2']);
        $this->assertEquals($settings->include_step_3, $this->testSettings['include_step_3']);
        $this->assertEquals($settings->timer_7_day, $this->testSettings['timer_7_day']);
        $this->assertEquals($settings->timer_14_day, $this->testSettings['timer_14_day']);
        $this->assertEquals($settings->timer_30_day, $this->testSettings['timer_30_day']);
        $this->assertEquals($settings->updated_at->setTimezone('UTC')->toDateTimeString(), $this->testSettings['updated_at']);

        // ensure duplicates are not created
        $testSettings                   = $this->testSettings;
        $testSettings['include_step_2'] = !$testSettings['include_step_2'];
        $testSettings['include_step_3'] = !$testSettings['include_step_3'];
        $testSettings['timer_7_day']    = !$testSettings['timer_7_day'];
        $testSettings['timer_14_day']   = !$testSettings['timer_14_day'];
        $testSettings['timer_30_day']   = !$testSettings['timer_30_day'];
        $testSettings['updated_at']     = '2017-01-16 06:27:12';

        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => $testSettings,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $settingsQuery = UserSetting::where('user_id', config('test.id'));

        $count = $settingsQuery->count();

        $this->assertEquals($count, 1);

        $settings = $settingsQuery->first();

        $this->assertEquals($settings->user_id, config('test.id'));
        $this->assertEquals($settings->include_step_2, $testSettings['include_step_2']);
        $this->assertEquals($settings->include_step_3, $testSettings['include_step_3']);
        $this->assertEquals($settings->timer_7_day, $testSettings['timer_7_day']);
        $this->assertEquals($settings->timer_14_day, $testSettings['timer_14_day']);
        $this->assertEquals($settings->timer_30_day, $testSettings['timer_30_day']);
        $this->assertEquals($settings->updated_at->setTimezone('UTC')->toDateTimeString(), $testSettings['updated_at']);
    }

    public function testSetUserSettingsDefaultValues()
    {
        // start fresh
        $this->clearUserSettings();

        $token    = $this->getToken();
        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $settings = UserSetting::where('user_id', config('test.id'))->first();

        $this->assertTrue(is_object($settings));
        $this->assertEquals($settings->user_id, config('test.id'));
        $this->assertEquals($settings->include_step_2, false);
        $this->assertEquals($settings->include_step_3, false);
        $this->assertEquals($settings->timer_7_day, false);
        $this->assertEquals($settings->timer_14_day, false);
        $this->assertEquals($settings->timer_30_day, false);
    }

    public function testGetUserSettingsInvalidInput()
    {
        // start fresh
        $this->clearUserSettings();

        $testSettings                   = $this->testSettings;
        $testSettings['include_step_2'] = 'Not a Boolean';
        $testSettings['include_step_3'] = 15;
        $testSettings['timer_7_day']    = true;
        $testSettings['timer_14_day']   = 0;
        $testSettings['timer_30_day']   = -24;

        $token    = $this->getToken();
        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => $testSettings,
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->include_step_2));
        $this->assertTrue(count($json->errors->include_step_2) > 0);
        $this->assertTrue(is_array($json->errors->include_step_3));
        $this->assertTrue(count($json->errors->include_step_3) > 0);
        $this->assertTrue(is_array($json->errors->timer_30_day));
        $this->assertTrue(count($json->errors->timer_30_day) > 0);
    }

    public function testGetUserSettingsEmpty()
    {
        // start fresh
        $this->clearUserSettings();

        $token    = $this->getToken();
        $response = $this->client->get('users/settings?token=' . urlencode($token));

        // should create and return new settings with default values
        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $settings = UserSetting::where('user_id', config('test.id'))->first();

        $this->assertTrue(is_object($settings));
        $this->assertEquals($settings->user_id, config('test.id'));
        $this->assertEquals($settings->include_step_2, false);
        $this->assertEquals($settings->include_step_3, false);
        $this->assertEquals($settings->timer_7_day, false);
        $this->assertEquals($settings->timer_14_day, false);
        $this->assertEquals($settings->timer_30_day, false);
    }

    public function testGetUserSettings()
    {
        $this->createUserSettings();

        $token    = $this->getToken();
        $response = $this->client->get('users/settings?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->user_id, config('test.id'));
        $this->assertEquals($json->data->include_step_2, $this->testSettings['include_step_2']);
        $this->assertEquals($json->data->include_step_3, $this->testSettings['include_step_3']);
        $this->assertEquals($json->data->timer_7_day, $this->testSettings['timer_7_day']);
        $this->assertEquals($json->data->timer_14_day, $this->testSettings['timer_14_day']);
        $this->assertEquals($json->data->timer_30_day, $this->testSettings['timer_30_day']);
    }

    public function testCorrectUpdatedTimestampBeforeDaylightSavingsTime()
    {
        // start fresh
        $this->clearUserSettings();

        $testSettings               = $this->testSettings;
        $testSettings['updated_at'] = '2017-01-20 08:06:21';

        $token    = $this->getToken();
        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => $testSettings,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testSettings['updated_at']);

        $token    = $this->getToken();
        $response = $this->client->get('users/settings?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testSettings['updated_at']);
    }

    public function testCorrectUpdatedTimestampAfterDaylightSavingsTime()
    {
        // start fresh
        $this->clearUserSettings();

        $testSettings               = $this->testSettings;
        $testSettings['updated_at'] = '2017-03-25 08:06:21';

        $token    = $this->getToken();
        $response = $this->client->post('users/settings?token=' . urlencode($token), [
            'form_params' => $testSettings,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testSettings['updated_at']);

        $token    = $this->getToken();
        $response = $this->client->get('users/settings?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $testSettings['updated_at']);
    }
}

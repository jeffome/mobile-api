<?php

namespace Tests\Feature;

class ProductTest extends TestCase
{
    /**
     * Tests for Signup Endpoint
     */
    public function testUnauthorizedProductsRequest()
    {
        $response = $this->client->get('products');

        $this->assertEquals(self::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testAuthorizedProductsRequest()
    {
        $token = $this->getToken();
        $response = $this->client->get('products?token=' . urlencode($token));

        $this->assertEquals(self::HTTP_OK, $response->getStatusCode());
    }

    public function testProductAccess()
    {
        $token = $this->getToken();
        $response = $this->client->get('product-access?token=' . urlencode($token));

        $this->assertEquals(self::HTTP_OK, $response->getStatusCode());

        $json = json_decode($response->getBody());
        $this->assertEquals($json->data->success, true);
        $this->assertTrue(property_exists($json->data->access, 'flashback'));
        $this->assertTrue(property_exists($json->data->access, 'intern_bootcamp'));
        $this->assertTrue(property_exists($json->data->access->subscriptions, 'basic-sciences'));
        $this->assertTrue(property_exists($json->data->access->subscriptions, 'clinical'));
    }

}
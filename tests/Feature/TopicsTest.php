<?php

namespace Tests\Feature;

class TopicsTest extends TestCase
{
    public function testNotAuthenticated()
    {
        $response = $this->client->get('topics');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testIndex()
    {
        $token = $this->getToken();
        $response = $this->client->get('topics?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
        $this->assertGreaterThan(0, count($json->data));
    }

    /** @deprecated  */
    public function testGetTopicAudio()
    {
        $topicId = 1;
        $token = urlencode($this->getToken());
        $response = $this->client->get("topic/asset/audio/$topicId?token=$token");

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
    }

    public function testGetTopicVideoUrl()
    {
        $topicId = 1;
        $token = urlencode($this->getToken());
        $response = $this->client->get("topic/asset-url/video/$topicId?token=$token");

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
    }

    public function testConfirmDownload()
    {
        $topicId = 1;
        $token = urlencode($this->getToken());
        $response = $this->client->post("confirm-download/video/$topicId?token=$token");

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
    }
}

<?php

namespace Tests\Feature;

use App\DailyAnswerTally;
use App\User;
use Carbon\Carbon;

class DailyAnswerTallyTest extends TestCase
{
    private function mockTallies($count = 1, $totalSeed = false)
    {
        $user = User::find(config('test.id'));
        $this->deleteUsersTallies();

        for ($i = 1; $i <= $count; $i++) {
            $date = Carbon::now()->subDays($i - 1);

            $total = $totalSeed;
            if ($totalSeed === false) {
                $total = rand(1, 100);
            }

            DailyAnswerTally::create([
                'user_id'    => config('test.id'),
                'total'      => $total,
                'flipped_on' => $date->toDateString(),
                'track_id'   => $user->career_track_id,
            ]);
        }
    }

    private function deleteUsersTallies()
    {
        return DailyAnswerTally::where('user_id', config('test.id'))->delete();
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('metrics/daily-answers');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testGetTodaysAnswerTallyDefaultZero()
    {
        $this->deleteUsersTallies();

        // verify no tally for today
        $tally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->first();

        $this->assertNull($tally);

        $token    = $this->getToken();
        $response = $this->client->get('metrics/daily-answers?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals(0, $json->data->todays_total);
        $this->assertEquals(Carbon::now()->setTimeZone('UTC')->toDateString(), $json->data->flipped_on);

        // verify no tally for today
        $tally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->first();

        $this->assertNotNull($tally);
        $this->assertEquals(0, $tally->total);

        // cleanup
        $this->deleteUsersTallies();
    }

    public function testGetTodaysAnswerTallyWithValue()
    {
        $this->mockTallies(10);

        $tally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->firstOrFail();

        $this->assertNotNull($tally);
        $this->assertNotEquals($tally->total, 0);

        $token    = $this->getToken();
        $response = $this->client->get('metrics/daily-answers?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($tally->total, $json->data->todays_total);
        $this->assertEquals(Carbon::now()->setTimeZone('UTC')->toDateString(), $json->data->flipped_on);

        // cleanup
        $this->deleteUsersTallies();
    }

    public function testGetLocalizedToday()
    {
        $this->mockTallies();

        $token    = $this->getToken();
        $response = $this->client->get('metrics/daily-answers?utc_offset_minutes=-1380&token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals(Carbon::now()->setTimeZone('UTC')->subDay()->toDateString(), $json->data->flipped_on);

        // cleanup
        $this->deleteUsersTallies();
    }

    public function testIncrementTodaysTallyWithInvalidDelta()
    {
        $this->deleteUsersTallies();
        $token = $this->getToken();

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => -1,
                'utc_offset_minutes' => 0,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->delta));
        $this->assertTrue(count($json->errors->delta) > 0);

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => 'some string',
                'utc_offset_minutes' => 0,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->delta));
        $this->assertTrue(count($json->errors->delta) > 0);

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => ['utc_offset_minutes' => 0],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->delta));
        $this->assertTrue(count($json->errors->delta) > 0);

    }

    public function testIncrementTodaysTallyWithInvalidUTCOffset()
    {
        $this->deleteUsersTallies();
        $token = $this->getToken();

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => 15,
                'utc_offset_minutes' => 'xxx',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->utc_offset_minutes));
        $this->assertTrue(count($json->errors->utc_offset_minutes) > 0);

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta' => 15,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->utc_offset_minutes));
        $this->assertTrue(count($json->errors->utc_offset_minutes) > 0);
    }

    // TODO: don't understand why this test fails on the total=60 assertion
    public function testIncrementTodaysTally()
    {
        $this->deleteUsersTallies();
        $token = $this->getToken();

        $incrementValue = 20;
        $iterations     = 5;
        $utc_offset_minutes = 60;

        for ($i = 0; $i < $iterations; $i++) {
            $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
                'form_params' => [
                    'delta'              => $incrementValue,
                    'utc_offset_minutes' => $utc_offset_minutes
                ],
            ]);

            $this->assertEquals(
                self::HTTP_OK,
                $response->getStatusCode()
            );
        }

        $tally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->setTimeZone('UTC')->toDateString())
            ->first();

        $this->assertEquals($incrementValue * $iterations, $tally->total);
        $this->assertEquals($utc_offset_minutes, $tally->utc_offset_minutes);
    }

    public function testOnlyOneAnswerTallyPerDay()
    {
        $this->mockTallies();

        $token = $this->getToken();

        $response = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => 20,
                'utc_offset_minutes' => 0,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));

        $tallyCount = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->count();

        $this->assertEquals(1, $tallyCount);

        // cleanup
        $this->deleteUsersTallies();
    }

    public function testGetAllTimeHighTally()
    {
        $this->mockTallies(100);

        $tally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('total', DailyAnswerTally::where('user_id', config('test.id'))->max('total'))
            ->firstOrFail();

        $this->assertNotNull($tally);
        $this->assertNotEquals($tally->total, 0);

        $token    = $this->getToken();
        $response = $this->client->get('metrics/daily-answers?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($tally->total, $json->data->all_time_total);

        // cleanup
        $this->deleteUsersTallies();
    }

    // TODO: not sure what the expected behavior is here?
    public function testTimeZoneLocalization()
    {
        $this->mockTallies(3, 0);

        $todayTally1 = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->first();

        $yesterdayTally1 = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->toDateString())
            ->first();

        $token = $this->getToken();

        $resp1 = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => 5,
                'utc_offset_minutes' => 0,
            ],
        ]);

        $resp2 = $this->client->post('metrics/daily-answers?token=' . urlencode($token), [
            'form_params' => [
                'delta'              => 5,
                'utc_offset_minutes' => -1380, // -23 hours
            ],
        ]);

        $todayTally2 = DailyAnswerTally::find($todayTally1->id);
        $yesterdayTally2 = DailyAnswerTally::find($yesterdayTally1->id);

        // cleanup
        $this->deleteUsersTallies();

        $this->assertEquals(0, $todayTally1->total);
        $this->assertEquals(0, $yesterdayTally1->total);

        $this->assertEquals(
            self::HTTP_OK,
            $resp1->getStatusCode()
        );

        $this->assertEquals(
            self::HTTP_OK,
            $resp2->getStatusCode()
        );

        $this->assertEquals(5, $todayTally2->total, 'todayTally2->total');
        $this->assertEquals(5, $yesterdayTally2->total, 'yesterdayTally2->total');
    }

    public function testSetAlertShownInvalidInput()
    {
        $this->mockTallies(3, 0);
        $token = $this->getToken();

        $response = $this->client->post('metrics/daily-answers/set-alert?token=' . urlencode($token), [
            'form_params' => [
                'alert_shown_at' => 'xxx',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->alert_shown_at));
        $this->assertTrue(count($json->errors->alert_shown_at) > 0);

        $response = $this->client->post('metrics/daily-answers/set-alert?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'errors'));
        $this->assertTrue(is_array($json->errors->alert_shown_at));
        $this->assertTrue(count($json->errors->alert_shown_at) > 0);

        // cleanup
        $this->deleteUsersTallies();
    }

    public function testSetAlertShown()
    {
        $this->mockTallies();

        $todayTally = DailyAnswerTally::where('user_id', config('test.id'))
            ->where('flipped_on', Carbon::now()->setTimeZone('UTC')->toDateString())
            ->first();

        $this->assertNull($todayTally->alert_shown_at);

        $token = $this->getToken();

        $response = $this->client->post('metrics/daily-answers/set-alert?token=' . urlencode($token), [
            'form_params' => [
                'alert_shown_at' => Carbon::now()->setTimeZone('UTC')->subHour()->toDateTimeString(),
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'success'));

        $todayTally = DailyAnswerTally::find($todayTally->id);

        $this->assertNotNull($todayTally->alert_shown_at);

        // cleanup
        $this->deleteUsersTallies();
    }

    // TODO: failing, see side effect is User.getAnswerTallyForDay
    public function testTimestampsAfterDaylightSavingsTime()
    {
        $alertTime = Carbon::now()->subHour()->toDateTimeString();

        $this->mockTallies();

        $token = $this->getToken();
        $resp1 = $this->client->post('metrics/daily-answers/set-alert?token=' . urlencode($token), [
            'form_params' => [
                'alert_shown_at' => $alertTime,
            ],
        ]);
        $json1 = json_decode($resp1->getBody());

        $resp2 = $this->client->get('metrics/daily-answers?token=' . urlencode($token));
        $json2 = json_decode($resp2->getBody());

        // cleanup
        $this->deleteUsersTallies();

        $this->assertEquals(
            self::HTTP_OK,
            $resp1->getStatusCode()
        );
        $this->assertTrue(property_exists($json1, 'success'));

        $this->assertEquals(
            self::HTTP_OK,
            $resp2->getStatusCode()
        );
        $this->assertTrue(property_exists($json2, 'data'));
        $this->assertEquals($alertTime, $json2->data->alert_shown_at);
        $this->assertEquals(strtotime($alertTime), strtotime($json2->data->alert_shown_at));
    }
}

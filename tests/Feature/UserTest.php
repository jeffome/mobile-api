<?php

namespace Tests\Feature;

use App\Social;
use App\User;
use Carbon\Carbon;

class UserTest extends TestCase
{
    public function testNotAuthenticated()
    {
        $response = $this->client->post('users/set-trial-end', [
            'form_params' => [
                'date' => 'invalid?',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testNoTrialDate()
    {
        $token    = $this->getToken();
        $response = $this->client->post('users/set-trial-end?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
    }

    public function testInvalidTrialDate()
    {
        $token    = $this->getToken();
        $response = $this->client->post('users/set-trial-end?token=' . urlencode($token), [
            'form_params' => [
                'date' => 'invalid?',
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'error'));
    }

    public function testValidTrialDate()
    {
        $testDate = '06/08/2020';
        $token    = $this->getToken();
        $response = $this->client->post('users/set-trial-end?token=' . urlencode($token), [
            'form_params' => [
                'date' => $testDate,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
    }

    public function testSetAvatar()
    {
        $avatar = 'https://www.gravatar.com/avatar/?d=mm';

        $token    = $this->getToken();
        $response = $this->client->post('users/set-avatar?token=' . urlencode($token), [
            'form_params' => [
                'profile_photo_url' => $avatar,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertTrue(property_exists($json->data, 'profile_photo_url'));
        $this->assertEquals($json->data->profile_photo_url, $avatar);
    }

    public function testCreateNewSocialLink()
    {
        $testingSocialToken = 'xyzzyx';
        $testingProvider    = 'facebook';
        $testingSocialId    = '098765432';
        $avatar             = 'https://www.gravatar.com/avatar/?d=mm';

        // ensure we're clean
        $rows = Social::where('user_id', config('test.id'))
            ->where('provider', $testingProvider)
            ->delete();

        $token    = $this->getToken();
        $response = $this->client->post('users/link-social?token=' . urlencode($token), [
            'form_params' => [
                'provider'          => $testingProvider,
                'uid'               => $testingSocialId,
                'oauth2_token'      => $testingSocialToken,
                'profile_photo_url' => $avatar,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'success'));
        $social = Social::where('user_id', config('test.id'))
            ->where('provider', $testingProvider)
            ->first();

        $this->assertTrue(is_object($social));
        $this->assertEquals($testingSocialToken, $social->oauth2_access_token);
        $this->assertEquals($testingSocialId, $social->uid);

        // cleanup
        $social->delete();

        $user = User::findOrFail(config('test.id'));
        $this->assertEquals($avatar, $user->imageurl);
    }

    public function testRemoveSocialLink()
    {
        $testingSocialToken = 'abc123defg456hij';
        $testingProvider    = 'facebook';
        $testingSocialId    = '0123456789123456789';

        // ensure we have a social to be deleted
        $social = Social::create([
            'user_id'             => config('test.id'),
            'provider'            => $testingProvider,
            'uid'                 => $testingSocialId,
            'oauth2_access_token' => $testingSocialToken,
        ]);

        $token    = $this->getToken();
        $response = $this->client->post('users/unlink-social?token=' . urlencode($token), [
            'form_params' => [
                'provider' => $testingProvider,
            ],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'success'));
        $social = Social::where('user_id', config('test.id'))
            ->where('provider', $testingProvider)
            ->first();

        $this->assertTrue(!is_object($social));
    }

    public function testEnsureSocialLinksAreNotUpdated()
    {
        $testingToken    = 'asdfghjkllkjhgfdsa';
        $testingProvider = 'facebook';
        $testingSocialId = '098765432';

        Social::where('user_id', config('test.id'))
            ->where('provider', $testingProvider)
            ->delete();
        $socialBefore = Social::create([
            'user_id'             => config('test.id'),
            'uid'                 => $testingSocialId,
            'provider'            => $testingProvider,
            'oauth2_access_token' => $testingToken,
            'oauth2_expires'      => Carbon::now()->addMonths(2),
        ]);

        $token = $this->getToken();
        $response = $this->client->post('users/link-social?token=' . urlencode($token), [
             'form_params' => [
                'provider'     => $testingProvider,
                'uid'          => '2134',
                'oauth2_token' => 'aiogfdskuljxjcuygrhxtyyetzhjyku',
             ],
        ]);
        $json = json_decode($response->getBody());

        $socialAfter = Social::where('user_id', config('test.id'))
            ->where('provider', $testingProvider)
            ->first();

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );

        // ensure nothing changed
        $this->assertEquals($socialBefore->provider, $socialAfter->provider);
        $this->assertEquals($socialBefore->uid, $socialAfter->uid);
        $this->assertEquals($socialBefore->oauth2_access_token, $socialAfter->oauth2_access_token);

        $this->assertTrue(property_exists($json, 'error'));

        // cleanup
        $socialBefore->delete();
    }
}

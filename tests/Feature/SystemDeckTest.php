<?php

namespace Tests\Feature;

use App\Flashcard;
use App\SystemDeck;
use App\SystemDeckType;

class SystemDeckTest extends TestCase
{
    private $flashcardsPerDeck;

    private function createTestDecks()
    {
        $this->deleteTestDecks();

        $flashcard_ids = [1, 3, 5, 7, 9, 10, 8, 6, 4, 2];
        $this->flashcardsPerDeck = count($flashcard_ids);

        $deckTypes = SystemDeckType::where('is_configurable', true)->get();
        foreach ($deckTypes as $deckType) {
            $deck = SystemDeck::create([
                'flashcard_system_deck_type_id' => $deckType->id,
                'user_id'                       => config('test.id'),
            ]);

            // remove any lingering associations
            $deck->flashcards()->sync([]);

            foreach ($flashcard_ids as $flashcard_id) {
                if ($flashcard = Flashcard::find($flashcard_id)) {
                    $deck->flashcards()->attach($flashcard->id);
                }
            }
        }
    }

    private function deleteTestDecks()
    {
        $decks = SystemDeck::where('user_id', config('test.id'))->get();
        foreach ($decks as $deck) {
            // remove any lingering associations
            $deck->flashcards()->sync([]);
            $deck->delete();
        }
    }

    public function testNotAuthenticated()
    {
        $response = $this->client->get('system-decks');

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testGetInvalidSystemDeckType()
    {
        $this->deleteTestDecks();
        $token = $this->getToken();
        $deckId = '1010101010101';
        $response = $this->client->get('system-decks/' . $deckId . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testGetAllSystemDecksForUserWithSystemDecks()
    {
        $this->createTestDecks();
        $token = $this->getToken();
        $response = $this->client->get('system-decks/?token=' . urlencode($token));
        $json = json_decode($response->getBody());
        $deckTypeIds = SystemDeckType::where('is_configurable', true)->get()->pluck('id')->toArray();
        $this->deleteTestDecks();

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);

        foreach ($json->data as $deck) {
            $this->assertContains($deck->system_deck_type_id, $deckTypeIds);
            $this->assertGreaterThan(0, count($deck->flashcard_ids));
            $this->assertEquals(config('test.id'), $deck->user_id);
        }
    }

    public function testGetAllSystemDecksForUserWithoutSystemDecks()
    {
        $this->deleteTestDecks();

        $token = $this->getToken();
        $response = $this->client->get('system-decks/?token=' . urlencode($token));
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertInternalType('array', $json->data);
    }

    public function testEmptyShowForEachDeckType()
    {
        $this->deleteTestDecks();
        $token = $this->getToken();
        $deckTypes = SystemDeckType::where('is_configurable', true)->get();

        foreach ($deckTypes as $deckType) {
            $response = $this->client->get('system-decks/' . $deckType->id . '?token=' . urlencode($token));
            $this->assertEquals(
                self::HTTP_OK,
                $response->getStatusCode()
            );
        }
    }

    public function testNotEmptyShowForEachDeckType()
    {
        $this->createTestDecks();
        $token = $this->getToken();
        $deckTypes = SystemDeckType::where('is_configurable', true)->get();

        foreach ($deckTypes as $deckType) {
            $response = $this->client->get('system-decks/' . $deckType->id . '?token=' . urlencode($token));
            $json = json_decode($response->getBody());
            $this->assertEquals(
                self::HTTP_OK,
                $response->getStatusCode()
            );
            $this->assertTrue(property_exists($json, 'data'));
            $this->assertEquals($deckType->id, $json->data->system_deck_type_id);
            $this->assertCount($this->flashcardsPerDeck, $json->data->flashcard_ids);
        }
    }

    public function testUnsuccessfulShowForNonConfigurableTypes()
    {
        $this->deleteTestDecks();
        $type = SystemDeckType::where('is_configurable', false)->first();
        $token = $this->getToken();
        $response = $this->client->get('system-decks/' . $type->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
    }

    public function testSuccessfulUpdateWithArray()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();
        $flashcardIds = [9, 8, 7];
        $date = '2013-12-01 08:06:21';

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => [
                'flashcard_ids' => $flashcardIds,
                'updated_at'    => $date,
            ],
        ]);
        $json = json_decode($response->getBody());

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $deck = SystemDeck::where('user_id', config('test.id'))
            ->where('flashcard_system_deck_type_id', $type->id)
            ->firstOrFail();

        $numFlashcards = count($flashcardIds);
        $this->assertTrue(property_exists($json, 'data'));
        $this->assertNotEquals(strtotime($json->data->updated_at), strtotime($deck->updated_at)); // because of UTC conversion
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($date));
        $this->assertCount($numFlashcards, $json->data->flashcard_ids);
        $this->assertCount($numFlashcards, $deck->flashcards);

        foreach ($json->data->flashcard_ids as $id) {
            $this->assertContains($id, $flashcardIds);
        }

        $this->deleteTestDecks();
    }

    public function testUpdateWithMissingType()
    {
        $this->createTestDecks();

        $token = $this->getToken();
        $response = $this->client->put('system-decks/?token=' . urlencode($token), [
            'form_params' => [
                'flashcard_ids' => [1, 3, 4],
            ],
        ]);

        $this->assertEquals(
            self::HTTP_METHOD_NOT_ALLOWED,
            $response->getStatusCode()
        );

        $this->deleteTestDecks();
    }

    public function testRemoveAllFlashcardsMissingFlashcardsID()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();

        $deck = SystemDeck::where('user_id', config('test.id'))->where('flashcard_system_deck_type_id', $type->id)->firstOrFail();

        // ensure we begin with more than 0 flashcards
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) > 0);

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => [],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $deck = SystemDeck::findOrFail($deck->id);
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) == 0);

        $this->deleteTestDecks();
    }

    public function testRemoveAllFlashcardsEmptyStringFlashcardsID()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();

        $deck = SystemDeck::where('user_id', config('test.id'))->where('flashcard_system_deck_type_id', $type->id)->firstOrFail();

        // ensure we begin with more than 0 flashcards
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) > 0);

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => ['flashcard_ids' => ''],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $deck = SystemDeck::findOrFail($deck->id);
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) == 0);

        $this->deleteTestDecks();
    }

    public function testRemoveAllFlashcardsEmptyArrayFlashcardsID()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();

        $deck = SystemDeck::where('user_id', config('test.id'))->where('flashcard_system_deck_type_id', $type->id)->firstOrFail();

        // ensure we begin with more than 0 flashcards
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) > 0);

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => ['flashcard_ids' => []],
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $deck = SystemDeck::findOrFail($deck->id);
        $this->assertTrue(count($deck->flashcards()->get()->pluck('id')->toArray()) == 0);

        $this->deleteTestDecks();
    }

    public function testFailedUpdateForNonConfigurableTypes()
    {
        $type = SystemDeckType::where('is_configurable', false)->firstOrFail();

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => [
                'flashcard_ids' => [1, 3, 4],
            ],
        ]);

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }

    public function testCorrectUpdatedTimestampBeforeDaylightSavingsTime()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();

        $deckAttrs = [
            'flashcard_ids' => [9, 8, 7],
            'updated_at'    => '2017-01-20 08:06:21',
        ];

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => $deckAttrs,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $deckAttrs['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($deckAttrs['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('system-decks/' . $type->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $deckAttrs['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($deckAttrs['updated_at']));

        $this->deleteTestDecks();
    }

    public function testCorrectUpdatedTimestampAfterDaylightSavingsTime()
    {
        $this->createTestDecks();
        $type = SystemDeckType::where('is_configurable', true)->firstOrFail();

        $deckAttrs = [
            'flashcard_ids' => [9, 8, 7],
            'updated_at'    => '2017-03-25 08:06:21',
        ];

        $token = $this->getToken();
        $response = $this->client->put('system-decks/' . $type->id . '?token=' . urlencode($token), [
            'form_params' => $deckAttrs,
        ]);

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $deckAttrs['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($deckAttrs['updated_at']));

        // now let's fetch the same deck and make sure we get the same timestamps
        $response = $this->client->get('system-decks/' . $type->id . '?token=' . urlencode($token));

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );

        $json = json_decode($response->getBody());

        $this->assertTrue(property_exists($json, 'data'));
        $this->assertEquals($json->data->updated_at, $deckAttrs['updated_at']);
        $this->assertEquals(strtotime($json->data->updated_at), strtotime($deckAttrs['updated_at']));

        $this->deleteTestDecks();
    }
}

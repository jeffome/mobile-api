<?php

namespace Tests\Feature;

class ProfessionTest extends TestCase
{
    public function testGetAllProfessions()
    {
        $response = $this->client->get('professions');

        $this->assertEquals(
            self::HTTP_OK,
            $response->getStatusCode()
        );
        $res = json_decode($response->getBody(), true);
        $testData = $res['data'];
        $this->assertTrue(is_array($testData));
        $this->assertTrue(count($testData) > 0);
    }

    public function testGetAllProfessions404()
    {
        $response = $this->client->get('professions/IDONOTEXIST');

        $this->assertEquals(
            self::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
    }
}

#!/bin/bash
set -e

echo "***** Code Sniffing *****";
vendor/bin/phpcs --config-set ignore_errors_on_exit 1
vendor/bin/phpcs --config-set ignore_warnings_on_exit 1
vendor/bin/phpcs --report=checkstyle --report-file=checkstyle-results.xml --standard=phpcs.xml
sleep 1;

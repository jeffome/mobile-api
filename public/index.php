<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
 */

require __DIR__ . '/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
 */

$app = require_once __DIR__ . '/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
 */

// If the header is set
if (isset($_SERVER['HTTP_BLACKFIRETRIGGER'])) {
    $config = new \Blackfire\ClientConfiguration();
    $config->setClientId("1c5bb23f-2b24-4f64-bd82-ba50a00b2b23");
    $config->setClientToken("129f6578f42c39f47c1fa613dc89b258ed472bda0b033abbdc39fafdc3782997");
    
    // let's create a client
    $blackfire = new \Blackfire\Client($config);
    // then start the probe
    $probe = $blackfire->createProbe();

    // When runtime shuts down, let's finish the profiling session
    register_shutdown_function(function () use ($blackfire, $probe) {
        // See the PHP SDK documentation for using the $profile object
        $profile = $blackfire->endProbe($probe);
        exit();
    });
}

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
